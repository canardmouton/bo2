package controller;

import Service.ServicesKit.MyHttpPost;
import Service.ServicesKit.MyHttpPostFile;
import Service.tools.FileService;
import Service.tools.SendFile;
import com.google.gson.Gson;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;

import java.io.File;
import java.io.IOException;

/**
 * Created by claebo_c on 23/01/17.
 */
public class SendController {

    FileService fileService = new FileService();

    public String sendMsg(String targetName, int targetInstance, String fctName, String data) throws IOException {
        System.out.println("SenderController : sendMsg");
        if (HomeController.test.equals("test")){
            targetName = HomeController.appName;
            targetInstance = HomeController.instanceID;
            fctName = "test";
            System.out.println("result");
            System.out.println(data);
            System.out.println();
            System.out.println();

        }

        System.out.println("to " + targetName + " / " + targetInstance);
        MyHttpPost myHttpPost = new MyHttpPost(new HttpPost("http://" + HomeController.stip + "/api/msg?fct=" + fctName + "&target=" + targetName +"&targetInstance=" + targetInstance + ""),
                new StringEntity("data=" + data));
        System.out.println("http://" + HomeController.stip + "/api/msg?fct=" + fctName + "&target=" + targetName +"&targetInstance=" + targetInstance + "");
        return myHttpPost.execute();
    }

    public String sendService(String targetName, int targetInstance, String fctName, String data) throws IOException {
        System.out.println("SenderController : sendService");
        if (HomeController.test.equals("test")){
            targetName = HomeController.appName;
            targetInstance = HomeController.instanceID;
            fctName = "test";
            System.out.println("result");
            System.out.println(data);
            System.out.println();
            System.out.println();

        }
        System.out.println("to " + targetName + " / " + targetInstance);
        MyHttpPost myHttpPost = new MyHttpPost(new HttpPost("http://" + HomeController.stip + "/api/service?fct=" + fctName + "&target=" + targetName +"&targetInstance=" + targetInstance + ""),
                new StringEntity("data=" + data));
        return myHttpPost.execute();
    }

    public String sendFile(String targetName, int targetInstance, String fctName, String filePath) throws IOException {
        System.out.println("SenderController : sendFile");
        System.out.println("SendFile make");
        File fileLocation = new File(filePath);
        System.out.println("File object created");

        if (HomeController.test.equals("test")){
            targetName = HomeController.appName;
            targetInstance = HomeController.instanceID;
            fctName = "test";

            System.out.println("result");
            System.out.println(fileService.fileToString(filePath));
            System.out.println();
            System.out.println();
            System.out.println("target name : " + targetName);
            System.out.println("target instance : " + targetInstance);
            System.out.println("target fonction : " + fctName);
            System.out.println();

        }

        SendFile target = new SendFile(targetInstance, fctName, targetName);

        String targetUrl = "http://" + HomeController.stip + "/api/send_file?fct=" + fctName + "&target=" + targetName
                + "&targetInstance=" + targetInstance
                + "&sender=" + HomeController.appName + "&senderInstance=" + HomeController.instanceID;
        System.out.println("targetURL = " + targetUrl);

        MyHttpPostFile myHttpPostFile = new MyHttpPostFile(new HttpPost(targetUrl),
                new StringEntity("data=" + new Gson().toJson(target)), fileLocation);


//        MyHttpPostFile myHttpPostFile = new MyHttpPostFile(new HttpPost("http://" + HomeController.stip + "/api/send_file?fct="
//                + fctName + "&target=" + targetName + "&targetInstance=" + targetInstance + + "&sender=" + HomeController.appName + "&senderInstance=" + HomeController.instanceID;
//                new StringEntity("data=" + new Gson().toJson(target)), fileLocation);
        //return myHttpPostFile.execute();
        return "ok";
    }
}
