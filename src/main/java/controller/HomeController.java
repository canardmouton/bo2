package controller;

import Service.CaisseService;
import Service.ParseFunction;
import Service.ServicesKit.*;
import Service.tools.Agenda;
import Service.tools.FileService;
import Service.tools.SendFile;
import com.google.gson.Gson;
import dto.gestionCommerciale.BO_GC_5.DataBO_GC_5DTO;
import dto.gestionCommerciale.BO_GC_5.ListDataBO_GC_5DTO;
import dto.gestionCommerciale.BO_GC_5.ProductBO_GC_5DTO;
import dto.gestionCommerciale.BO_GC_5.StructBO_GC_5DTO;
import env.EnvEntity;
import model.ProductEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.expression.ParseException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by claebo_c on 13/09/16.
 */
@Controller
public class HomeController {
    public static int instanceID = 1;
    public static String appName = "BO";
    public static String appip = "192.168.1.13:8080";
    public static String stip = "192.168.1.13:8081";
    public static String test = "test";
    public static String pathFileTickets = null;
    public static List<EnvEntity> listCaisse = new ArrayList<>();
    public static EnvEntity gc = new EnvEntity("GC", 1);
    public static EnvEntity fi = new EnvEntity("FI", 1);
    public CaisseService caisseService = new CaisseService();

    private ParseFunction fctn = null;
    private FileService fileService = new FileService();

    private static final Logger LOG = LoggerFactory
            .getLogger(HomeController.class);

    public HomeController() {

        this.fctn = new ParseFunction("", "");
    }

    @RequestMapping(value = {"/", "/home"})
    public String home() {
        return "index";
    }

    @RequestMapping(value = "/api/getIp", method = RequestMethod.GET)
    public
    @ResponseBody
    String getAppIp() {
        JSONObject jo = new JSONObject();
        jo.put("test", this.test);
        jo.put("appName", this.appName);
        jo.put("appip", appip);
        jo.put("stip", stip);
        jo.put("instanceId", instanceID);
        return jo.toString();
    }

    @RequestMapping(value = "/api/settest", method = RequestMethod.POST)
    public ModelAndView setTest(HttpServletRequest request) {

        this.test = request.getParameter("inputtest");
        System.out.println(this.test);
        if (this.test.equals("test")) {
            listCaisse.add(new EnvEntity("CA1", 1));
            listCaisse.add(new EnvEntity("CA2", 2));
        }
        for (EnvEntity entity : this.listCaisse) {
            System.out.println(entity.getNom());
        }
        return new ModelAndView("redirect:/");
    }

    @RequestMapping(value = "/api/setappname", method = RequestMethod.POST)
    public ModelAndView setAppName(HttpServletRequest request) {
        this.appName = request.getParameter("inputappname");
        return new ModelAndView("redirect:/");
    }

    @RequestMapping(value = "/api/setappip", method = RequestMethod.POST)
    public ModelAndView setAppIp(HttpServletRequest request) {
        this.appip = request.getParameter("inputid");
        return new ModelAndView("redirect:/");
    }

    @RequestMapping(value = "/api/setstip", method = RequestMethod.POST)
    public ModelAndView setStIp(HttpServletRequest request) {
        this.stip = request.getParameter("inputstid");
        return new ModelAndView("redirect:/");
    }

    @RequestMapping(value = "/api/setinstanceid", method = RequestMethod.POST)
    public ModelAndView setInstandeId(HttpServletRequest request) {
        this.instanceID = Integer.parseInt(request.getParameter("inputinstanceid"));
        return new ModelAndView("redirect:/");
    }

    /**
     * Handshake function
     *
     * @return
     * @throws IOException
     * @throws org.json.simple.parser.ParseException
     */
    @RequestMapping(value = "/api/handshake", method = RequestMethod.GET)
    public
    @ResponseBody
    String handshake() throws IOException, org.json.simple.parser.ParseException {
        System.out.println("titi");
        String senderOut = System.getenv("AppName");
        if (senderOut == null) {
            senderOut = "BO";
        }

        /**
         * Creating the agenda
         */
        Agenda[] agendaOut = new Agenda[4];
        agendaOut[0] = new Agenda("07:00", 5, this.appip + "/BO/api/service?fct=promotionToCaisse");//BO-CA-1
        agendaOut[1] = new Agenda("08:00", 5, this.appip + "/BO/api/service?fct=produitToCaisse");//BO-CA-2
        agendaOut[2] = new Agenda("22:00", 5, this.appip + "/BO/api/service?fct=ticketToCommercial");//BO-GC-5
        agendaOut[3] = new Agenda("08:00", 5, this.appip + "/BO/api/service?fct=inventaireToUser");//BO-UT-1
        Handshake handOut = new Handshake(this.appName, this.instanceID, this.appip, agendaOut);

        /**
         * Running the post method to send the data to the st
         */
        MyHttpPost myHttpPost = new MyHttpPost(new HttpPost("http://" + this.stip + "/api/handshake"),
                new StringEntity("data=" + new Gson().toJson(handOut)));

        return myHttpPost.execute();
    }

    /**
     * Get the message and send a response
     *
     * @param fct
     * @param request
     * @return
     * @throws ParseException
     */
    @RequestMapping(value = "/api/msg", method = RequestMethod.POST)
    public
    @ResponseBody
    String getMessageGET(@RequestParam(required = true, value = "fct", defaultValue = "") String fct,
                         @RequestParam(required = true, value = "data", defaultValue = "") String data,
                         HttpServletRequest request)
            throws ParseException {
        System.out.println("Messages !!!!");
        System.out.println(" data after" + data);
        fctn.setFct(fct);
        fctn.setData(data);

        //appel parser de data en fonction du
        Response response = new Response(true, fctn.execute());
        return "data=" + new Gson().toJson(response);
    }

    /**
     * Get the service and send the result to the st
     *
     * @param fct
     * @param request
     * @return
     * @throws ParseException
     */
    @RequestMapping(value = "/api/service", method = RequestMethod.POST)
    public
    @ResponseBody
    String getServiceGET(@RequestParam(required = true, value = "fct", defaultValue = "") String fct,
                         @RequestParam(required = true, value = "data", defaultValue = "") String data,
                         HttpServletRequest request)
            throws ParseException {
        System.out.println("Service !!!!");
        System.out.println(" dqtq after" + data);


        fctn.setFct(fct);
        fctn.setData(data);

        WebService result = new WebService(this.instanceID);
        result.senderSet(this.appName);
        result.dataSet(new TrueData(fctn.execute()));
        return new Gson().toJson(result);
    }

    /**
     * Send file to the st
     *
     * @param targetName
     * @param targetInstance
     * @param fctName
     * @return
     * @throws IOException
     */
    public
    @ResponseBody
    String sendFile(String targetName, String targetInstance, String fctName, String filePath) throws IOException {
        System.out.println("HomeController : sendFile");

        SendFile target = new SendFile(1, fctName, targetName);
        System.out.println("SendFile make");
        File fileLocation = new File(filePath);
        System.out.println("File object created");
        String targetUrl = "http://" + HomeController.stip + "/api/send_file?fct=" + fctName + "&target=" + targetName
                + "&targetInstance=" + targetInstance
                + "&sender=" + HomeController.appName + "&senderInstance=" + HomeController.instanceID;
        System.out.println("targetURL = " + targetUrl);
        MyHttpPostFile myHttpPostFile = new MyHttpPostFile(new HttpPost(targetUrl),
                new StringEntity("data=" + new Gson().toJson(target)), fileLocation);

        System.out.println("end of sendFile");
        return myHttpPostFile.execute();
    }

    public String sendMsg(String targetName, int targetInstance, String fctName, String data) throws IOException {
        System.out.println("SenderController : sendMsg");
        if (HomeController.test.equals("test")){
            targetName = HomeController.appName;
            targetInstance = HomeController.instanceID;
            System.out.println("result");
            System.out.println(data);
            System.out.println();
            System.out.println();

        }

        System.out.println("to " + targetName + " / " + targetInstance);
        MyHttpPost myHttpPost = new MyHttpPost(new HttpPost("http://" + HomeController.stip + "/api/msg?fct=" + fctName + "&target=" + targetName +"&targetInstance=" + targetInstance + ""),
                new StringEntity("data=" + data));
        return myHttpPost.execute();
    }

    /**
     * Get file from st and store it into the application
     *
     * @return
     * @throws IOException
     */
    @RequestMapping(value = "/api/notif_file", method = RequestMethod.POST)
    public
    @ResponseBody
    String notifFile(@RequestParam(required = true, value = "fct", defaultValue = "") String fct,
                     @RequestParam(required = true, value = "data", defaultValue = "") String data,
                     HttpServletRequest request) throws IOException {
        System.out.println("notif File !!!!!!!!!");
        System.out.println(data);
        String data_result = fileService.downloadFile(data);
        System.out.println(data_result);
        this.fctn.setData(data_result);
        this.fctn.setFct(fct);
        String result = this.fctn.execute();
        return "data=" + result;
    }

    /*
        TEST PART
     */


    @RequestMapping(value = "/testMsg")
    public
    @ResponseBody
    String testSendMessage(HttpServletRequest request) throws IOException {
        StructBO_GC_5DTO struct = new StructBO_GC_5DTO();
        struct.setSender("sender");
        //struct.setDate("date");
        //struct.setYourApp("ZEESDF");
        //struct.setYourInstance(14);
        struct.setInstanceID(18);
        List<DataBO_GC_5DTO> listdata = new ArrayList<>();

        ProductBO_GC_5DTO promotion = new ProductBO_GC_5DTO("client", 14, true);
        ProductBO_GC_5DTO promotion2 = new ProductBO_GC_5DTO("id2", 15, false);
        List<ProductBO_GC_5DTO> listpromo = new ArrayList<>();

        listpromo.add(promotion);
        listpromo.add(promotion2);
        DataBO_GC_5DTO data1 = new DataBO_GC_5DTO("aztruc", listpromo);
        //data.setAdresse("ref");
        listdata.add(data1);

        //data.setStatus(true);
        ListDataBO_GC_5DTO listDataBO_gc_5DTO = new ListDataBO_GC_5DTO(listdata);
        struct.setData(listDataBO_gc_5DTO);

        String dataToSend = new Gson().toJson(struct);
        StringEntity toto = new StringEntity("data=" + dataToSend, "UTF-8");
        System.out.println("datatosend=" + dataToSend);
        System.out.println("data before =" + toto.getContent());//setter les products

        //String targetURL = "http://" + this.stip + "/api/notif_file?fct=testHugo&target=" + this.appName + "&targetInstance=" + this.instanceID;
        //MyHttpPost myHttpPost = new MyHttpPost(new HttpPost(targetURL), toto);
        //return myHttpPost.execute();
        return sendFile(this.appName, this.appip, "testHugo", "/Documents/back-office/test.json");
    }

    @RequestMapping(value = "/testService")
    public
    @ResponseBody
    String testService(HttpServletRequest request) throws IOException {
        TrueData trueData = new TrueData("toto");
        String data = new Gson().toJson(new Message(this.appName, this.instanceID, trueData));
        MyHttpPost myHttpPost = new MyHttpPost(new HttpPost("http://" + this.stip + "/api/service?fct=teste&target=" + this.appName + "&targetInstance=" + this.instanceID + ""),
                new StringEntity("data=" + data));
        return myHttpPost.execute();
    }

    @RequestMapping(value = "/testFile")
    public
    @ResponseBody
    String testSendFile() throws IOException {
        TrueData trueData = new TrueData("toto");
        String fctName = "test";
        String targetName = this.appName;
        String targetInstance = Integer.toString(this.instanceID);
        Message target = new Message(this.appName, this.instanceID, trueData);
        File fileLocation = new File("/project/upload-dir/hello.txt");
        MyHttpPostFile myHttpPostFile = new MyHttpPostFile(new HttpPost("http://" + this.stip + "/api/send_file?fct=" + fctName + "&target=" + targetName + "&targetInstance=" + targetInstance + ""),
                new StringEntity("data=" + new Gson().toJson(target)), fileLocation);
        return myHttpPostFile.execute();
    }

    @RequestMapping(value = "/checkFile")
    public
    @ResponseBody
    String checkFile() throws IOException {
        File fileLocation = new File("/project/upload-dir/hello.txt");
        File dir = new File("/project");
        File[] filesList = dir.listFiles();
        for (File file : filesList) {
            if (file.isDirectory()) {
                System.out.println(file.getName());
            }
        }
        return "{result : " + fileLocation.isFile() + " }";
    }


    @RequestMapping("404")
    public String handlePageNotFound(ModelMap model) {
        return "404";
    }

    public static String encode(String in) {
        String out = in;
        Charset.forName("UTF-8").encode(out);
        return out;
    }


    // FIXME WE ARE WORKING HERE NOW

    @RequestMapping(value = "/refToST")
    public
    @ResponseBody
    String sendRefST() throws IOException {
        System.out.println("Inside refToST");
        this.sendFile(HomeController.appName, "1", "productToBO", "/project/upload-dir/referentiel.json");

        return "toto";
    }

    @RequestMapping(value = "/env")
    public
    @ResponseBody
    ModelAndView setMyEnv() throws IOException {
        HomeController.instanceID = 1;
        HomeController.appName = "BO";
        HomeController.appip = System.getenv("DB_IP") + ":8090/BO";
        HomeController.stip = System.getenv("DB_IP") + ":3000";

        return new ModelAndView("redirect:/");
    }

    @RequestMapping(value = "/env2")
    public
    @ResponseBody
    ModelAndView setMyEnv2() throws IOException {
        HomeController.instanceID = 1;
        HomeController.appName = "BO";
        HomeController.appip = System.getenv("DB_IP") + ":8080/BO";
        HomeController.stip = System.getenv("DB_IP") + ":8081";

        return new ModelAndView("redirect:/");
    }

    @RequestMapping(value = "/env1")
    public
    @ResponseBody
    ModelAndView setMyEnv1() throws IOException {
        HomeController.instanceID = 1;
        HomeController.appName = "BO";
        HomeController.appip = System.getenv("DB_IP") + ":8080/BO";
        HomeController.stip = System.getenv("DB_IP") + ":8000";

        return new ModelAndView("redirect:/");
    }

    @RequestMapping(value = "/envProd")
    public
    @ResponseBody
    ModelAndView setMyEnvProd() throws IOException {
        HomeController.instanceID = 2;
        HomeController.appName = "BO";
        HomeController.appip = "192.168.0.152:8082/BO";
        HomeController.stip = "192.168.0.151";

        return new ModelAndView("redirect:/");
    }


    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public
    @ResponseBody
    String endConnection() throws IOException, org.json.simple.parser.ParseException {
        /**
         * Running the post method to send the data to the st
         */
        MyHttpPost myHttpPost = new MyHttpPost(new HttpPost("http://" + this.stip + "/api/end_connection"),
                new StringEntity("data=" + new Gson().toJson(new Message(appName, instanceID, new TrueData("")))));
        return myHttpPost.execute();
    }

    @RequestMapping(value = "/testInsertionSimpleProduct", method = RequestMethod.GET)
    public ModelAndView testInsertionSimpleProduct() {
        System.out.println("inside testInsertionSimpleProduct");
        EntityManagerFactory emf;
        EntityManager em;
        emf = Persistence.createEntityManagerFactory("bo2");
        em = emf.createEntityManager();


        ProductEntity productEntity = new ProductEntity("REF_RE", "REF_FO", 2, 1, 1.1, 1, 1, "FOURNI NAME", "DESC");

        em.getTransaction().begin();
        em.persist(productEntity);
        em.getTransaction().commit();

        System.out.println("END OF INSERTION");

        return new ModelAndView("redirect:/");
    }


// FIXME Making the flow here from now on

// 0 Créer le fichier test OK
// 1 Récupérer et enregistrer le fichier
// 2 Charger et printer le fichier
// 3 Modifier les informations
// 4 Faire le fichier de retour
// 5 Envoyer le fichier de retour

    @RequestMapping(value = "/reaToST")
    public
    @ResponseBody
    String sendReaST() throws IOException {
        LOG.debug("Inside reaToST");
        System.out.println("Inside reaToST");
        this.sendFile(HomeController.appName, "1", "commandToBO", "/project/upload-dir/reassort.json");

        return "toto";
    }

    @RequestMapping(value = "/retourLivraison")
    public
    @ResponseBody
    String sendMsgRL() throws IOException {
        System.out.println("Test retour livraison message");
        TrueData trueData = new TrueData("Test Msg");
        String data = new Gson().toJson(new Message(this.appName, this.instanceID, trueData));
        this.sendMsg(HomeController.appName, 1, "retourLivraisonToCommercial", data);

        return "toto";
    }

    @RequestMapping(value = "/productToCaisse")
    public
    @ResponseBody
    String sendMsgBO_CA_2() throws IOException {
        System.out.println("Test produit to Caisse message");
        TrueData trueData = new TrueData("Test produitToCaisse Msg");
        String data = caisseService.produitToCaisse();
        //this.sendMsg("CA", 1, "produitToCaisse", data);
        return data;
    }

    @RequestMapping(value = "/catalogToBack")
    public
    @ResponseBody
    String getandsentMsgBO_CA_4() throws IOException {
        System.out.println("Test catalogue to Caisse message");
        FileService fileService = new FileService();
        String trueData = fileService.fileToString("/project/upload-dir/CatalogCoupon.json");
        this.sendMsg(HomeController.appName, 1, "catalogToBack", trueData);
        return "Test";
    }

    @RequestMapping(value= "/promoToCaisse")
    public
    @ResponseBody
    String sendMsgBO_CA_1() throws IOException {
        System.out.println("Test promoToCaisse");
        caisseService = new CaisseService();
        String data = caisseService.promotionToCaisse();
        //this.sendMsg("CA", 1, "promotionToCaisse", data);
        return data;
    }

    @RequestMapping(value= "/livraisonToBO")
    public
    @ResponseBody
    String sendMsgGC_BO_2() throws IOException {
        System.out.println("Test livraison to BO message");
        String data = fileService.fileToString("/project/upload-dir/bonLivraison.json");
        this.sendMsg(HomeController.appName, HomeController.instanceID, "livraisonToBO", data);

        return "toto";
    }


    @RequestMapping(value= "/referentielToBO")
    public
    @ResponseBody
    String sendMsgRE_BO_1() throws IOException {
        System.out.println("test referentiel to bo message");
        String data = fileService.fileToString("/project/upload-dir/referentiel.json");
        this.sendFile(HomeController.appName, "1", "productToBO", "/project/upload-dir/referentiel.json");
        return data;
    }

    @RequestMapping(value= "/ticketToBO")
    public
    @ResponseBody
    String sendCA_BO_1() throws IOException {
        System.out.println("test ticket to bo");
        String data = fileService.fileToString("/project/upload-dir/ticket.json");
        this.sendMsg(HomeController.appName, HomeController.instanceID, "ticketToBO", data);
        return "toto";
    }
}

