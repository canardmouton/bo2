package Service.tools;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * Created by claebo_c on 22/01/17.
 */
public class MyDate {

    public static Date fromString (String dateString){
        DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm");
        Date newDate = new Date();
        try {
            newDate = dateFormat.parse(dateString);
            System.out.println("Parse Date ok");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        System.out.println(newDate);
        return newDate;
    }
}
