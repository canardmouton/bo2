package Service.tools;

import Service.ServicesKit.MyHttpGetFile;
import controller.HomeController;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Created by claebo_c on 23/01/17.
 */
public class FileService {

    /**
     * Create a temp file with name "<codeFlux>_<rand>.tmp", insert data string and return the path
      * @param codeFlux for name the file
     * @param data to insert
     * @return file path
     * @throws IOException
     */
    public String createTmpFile(String codeFlux, String data) throws IOException {
        System.out.print("Create tmp File :");
        File tmpFile = File.createTempFile(codeFlux + "_", ".tmp");
        FileOutputStream tmpOutputStream = new FileOutputStream(tmpFile);
        tmpOutputStream.write(data.getBytes());
        String filePath = tmpFile.getAbsolutePath();
        tmpOutputStream.close();
        System.out.print(filePath);
        return filePath;
    }

    /**
     * Create or increment temp file and return file path
     * @param path location
     * @param codeFlux for name the file
     * @param data to insert
     * @return file path
     * @throws IOException
     */
    public String incrementFile(String path, String codeFlux, String data) throws IOException {
        System.out.print("Increment tmp File :");
        File tmpFile = null;
        if (path == null){
            tmpFile = File.createTempFile(codeFlux + "_", ".tmp");
            HomeController.pathFileTickets = tmpFile.getAbsolutePath();
        } else {
            tmpFile = new File(HomeController.pathFileTickets);
        }
        FileOutputStream tmpOutputStream = new FileOutputStream(tmpFile, true);
        tmpOutputStream.write(data.getBytes());
        String filePath = tmpFile.getAbsolutePath();
        tmpOutputStream.close();
        System.out.println(filePath);
        return filePath;
    }

    /**
     * convert the content of file to string
     * @param path of file
     * @return data
     * @throws IOException
     */
    public String fileToString(String path) throws IOException{
        Charset encoding = StandardCharsets.UTF_8;
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, encoding);
    }

    /**
     * Download file form notif file and return string data
     * @param pathFile location where to get the file
     * @return String data
     * @throws IOException
     */
    public String downloadFile(String pathFile) throws IOException {
        JSONObject jsonObj = new JSONObject(pathFile);
        String file = jsonObj.get("fileID").toString().replaceAll("\"", "")
                .replaceAll("[\\[\\]\"]", "").replaceAll(" ", "+");

        String downloadFileUrl = "http://" + HomeController.stip + "/api/get_file?target=" + HomeController.appName
                + "&targetInstance=" + HomeController.instanceID + "&fileID=" + file;

        MyHttpGetFile myHttpGetFile = new MyHttpGetFile(downloadFileUrl, "/project/upload-dir/", file,
                Integer.toString(HomeController.instanceID));

        System.out.println("file downloaded");

        String targetFilePath = "http://" + HomeController.stip + "/api/get_file?target=" + HomeController.appName
                + "&targetInstance=" + HomeController.instanceID + "&fileID=" + file;
        System.out.println("Please download with this link : " + targetFilePath);

        String filepath = "/project/upload-dir/" + file;

        String result = myHttpGetFile.execute();

       return this.fileToString(filepath);


    }
}
