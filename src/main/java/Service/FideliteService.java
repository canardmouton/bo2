package Service;

import com.google.gson.Gson;
import controller.HomeController;
import controller.SendController;
import dto.caisse.BO_CA_4.DataBO_CA_4DTO;
import dto.caisse.BO_CA_4.StructBO_CA_4DTO;
import dto.caisse.CA_BO_1.StructCA_BO_1DTO;
import dto.fidelite.BO_FI_1.DataBO_FI_1DTO;
import dto.fidelite.BO_FI_1.StructBO_FI_1DTO;
import dto.fidelite.FI_BO_1.StructFI_BO_1DTO;
import dto.fidelite.FI_BO_2.StructFI_BO_2DTO;
import env.EnvEntity;
import org.springframework.stereotype.Service;

import java.io.IOException;

/**
 * Created by hugo on 07/01/17.
 */
@Service

public class FideliteService {

    private Gson gson = new Gson();
    private SendController sendController = new SendController();

    // What : envoie du ticket (isCommande)
    // When : ticket reçu de Caisse (CA-BO-1)
    // Form : Caisse
    // To : Fi
    // Type : Message
    // Code : BO-FI-1
    public String ticketToFidelite (StructCA_BO_1DTO data) throws IOException{
        StructBO_FI_1DTO structBO_fi_1DTO = new StructBO_FI_1DTO();
        structBO_fi_1DTO.setSender(HomeController.appName);
        structBO_fi_1DTO.setInstanceID(HomeController.instanceID);
        String caisseId = data.getSender() + "_"+ data.getInstanceID();
        structBO_fi_1DTO.setData(new DataBO_FI_1DTO(caisseId, data.getData()));

        String structResult = gson.toJson(structBO_fi_1DTO);

        return sendController.sendMsg(HomeController.fi.getNom(), HomeController.fi.getInstance(), "ticketToFidelite", structResult);
    }

    // What : coupon de fidelite
    // When : appel
    // Form : FI
    // To : couponToCaisse
    // Type : Message
    // Code : FI-BO-1
    public String clientToBack (String data) throws IOException{
        StructFI_BO_1DTO structFI_bo_1DTO = gson.fromJson(data, StructFI_BO_1DTO.class);
        System.out.println("Json send by Fidelite : \n" + data);
        return null;
    }

    // What : catalogue de coupons de fidelite à transmettre a la caisse (fct ?)
    // When : appel
    // Form : FI
    // To : caisse
    // Type : Message
    // Code : FI-BO-2
    public String catalogToBack (String data) throws IOException {
        StructFI_BO_2DTO structFI_bo_2DTO = gson.fromJson(data, StructFI_BO_2DTO.class);
        System.out.println("Json send by Fidelite : \n" + data);
        structFI_bo_2DTO.setSender(HomeController.appName);
        structFI_bo_2DTO.setInstanceID(HomeController.instanceID);
        listCouponToCaisse(structFI_bo_2DTO);
        return "catalogToBack send !";
    }

    // What : liste coupon de fidéleté
    // When : recu par Fi (clientToBack)
    // Form : BackOffice (Bo)
    // To : Caisse (listCouponToCa)
    // Type : Message
    // Code : BO-CA-4
    public String listCouponToCaisse(StructFI_BO_2DTO structFI_bo_2DTO) throws IOException{
        System.out.println("On envoie les coupons à la caisse : " + structFI_bo_2DTO.getData().getListe_coupons());
        DataBO_CA_4DTO dataBO_ca_4DTO = new DataBO_CA_4DTO(structFI_bo_2DTO.getData().getRefClient(), structFI_bo_2DTO.getData().getListe_coupons());
        StructBO_CA_4DTO structBO_ca_4DTO = new StructBO_CA_4DTO( HomeController.instanceID, HomeController.appName, dataBO_ca_4DTO);
        String result = "OK";
        System.out.println("Envoie du catalogue à la caisse n°1");
        String[] caisse = structFI_bo_2DTO.getData().getRefCaisse().split("_");
        int idcaisse = Integer.parseInt(caisse[0]);
        result = sendController.sendMsg("CA", idcaisse, "couponsToCA", gson.toJson(structBO_ca_4DTO));
        return result;
    }


}
