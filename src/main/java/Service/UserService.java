package Service;

import com.google.gson.Gson;
import dao.ClientDAO;
import dao.ProductDAO;
import dto.ListProductBoUtDTO;
import dto.ProductBoUtDTO;
import model.ClientEntity;
import model.ProductEntity;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by hugo on 07/01/17.
 */
@Service
public class UserService {

//    @Autowired
//    private HomeController homeController;

    //@Autowired
    //private Product2DAO product2DAO = new Product2DAO();

//    @Autowired
    private ProductDAO productDAO = new ProductDAO();
    private ClientDAO clientDAO = new ClientDAO();

    private Gson gson = new Gson();

    public UserService() {
    }



    public ListProductBoUtDTO inventaireToUser (String data) throws IOException {
        Collection<ProductEntity> productEntities = productDAO.findAll();
        List<ProductBoUtDTO> productBoUtDTOList = new ArrayList<ProductBoUtDTO>();
        ListProductBoUtDTO listProductBoUtDTO = new ListProductBoUtDTO();
        for(ProductEntity productEntity : productEntities){
            productBoUtDTOList.add(new ProductBoUtDTO(productEntity));
        }
        listProductBoUtDTO.setProductBoUtDTOS(productBoUtDTOList);
        return listProductBoUtDTO;
    }

    public void testAdd(){
        System.out.println("getProductREF");
        ProductEntity productEntity = new ProductEntity();
        productEntity = productDAO.getByRef("PcFixeHP56");
        System.out.println("pro : " + productEntity.getDescription());
        System.out.println("getProductID");
        ProductEntity productEntity2 = new ProductEntity();
        productEntity2 = productDAO.getById(4);
        System.out.println("pro init : " + productEntity2.getDescription());
        productEntity2.setDescription("pinpin descrip");
        productDAO.update(productEntity2);
        System.out.println("getProductID after update");
        ProductEntity productEntity3 = new ProductEntity();
        productEntity3 = productDAO.getById(4);
        System.out.println("pro after : " + productEntity3.getDescription());
        System.out.println("addClent");
        ClientEntity clientEntity = new ClientEntity();
        clientEntity.setRefClient("test adress");
        clientDAO.persist(clientEntity);

        ClientEntity entity = new ClientEntity();
        entity = clientDAO.getById(1);
        System.out.println("get : " + entity.getRefClient());
    }
}
