package Service.ServicesKit;

import com.google.gson.Gson;
import org.apache.commons.io.FileUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.net.URL;

/**
 * Created by nea on 13/11/16.
 */
public class MyHttpGetFile {
    String url;
    String target;
    String file;
    String instanceID;

    /**
     * Constructor of the class MyHttpPost
     * @param url
     * @param target
     */
    public MyHttpGetFile(String url, String target, String file, String instanceID) {
        this.url = url;
        this.target = target;
        this.file = file;
        this.instanceID = instanceID;
    }

    /**
     * Function that will execute a post function on the st
     * @return
     * @throws IOException
     */
    public String execute() throws IOException {
        URL getFileUrl = new URL(this.url);

        File f = new File(this.target + file);
        FileUtils.copyURLToFile(getFileUrl, f);
        return "ok";
    }

    public String getFile(MultipartFile file, String targetInstance) throws IOException {
        System.out.println("getFile !!!!");
        FileHandler fileHandler = new FileHandler(file);
        fileHandler.store(file, targetInstance);
        Response response = new Response(true, "Every thing works !");
        return "data=" + new Gson().toJson(response);
    }
}