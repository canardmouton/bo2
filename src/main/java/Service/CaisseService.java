package Service;

import Service.tools.FileService;
import com.google.gson.Gson;
import controller.HomeController;
import controller.SendController;
import dao.ClientDAO;
import dao.LocalPromotionDAO;
import dao.ProductDAO;
import dao.SimplePromotionDAO;
import dto.caisse.BO_CA_1.DataBO_CA_1DTO;
import dto.caisse.BO_CA_1.PromotionBO_CA_1DTO;
import dto.caisse.BO_CA_1.StructBO_CA_1DTO;
import dto.caisse.BO_CA_2.DataBO_CA_2DTO;
import dto.caisse.BO_CA_2.ProductBO_CA_2DTO;
import dto.caisse.BO_CA_2.StructBO_CA_2DTO;
import dto.caisse.BO_CA_4.DataBO_CA_4DTO;
import dto.caisse.BO_CA_4.StructBO_CA_4DTO;
import dto.caisse.CA_BO_1.DataCA_BO_1DTO;
import dto.caisse.CA_BO_1.PairCA_BO_1;
import dto.caisse.CA_BO_1.ProductCA_BO_1DTO;
import dto.caisse.CA_BO_1.StructCA_BO_1DTO;
import dto.fidelite.FI_BO_2.StructFI_BO_2DTO;
import env.EnvEntity;
import model.ClientEntity;
import model.ProductEntity;
import model.SimplePromotionEntity;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


/**
 * Created by hugo on 07/01/17.
 */
@Service

public class CaisseService {

    private FileService fileService = new FileService();
    private SendController sendController = new SendController();
    private CommercialService commercialService = new CommercialService();
    private FideliteService fideliteService = new FideliteService();
    private LocalPromotionDAO localPromotionDAO = new LocalPromotionDAO();
    private ProductDAO productDAO = new ProductDAO();
    private ClientDAO clientDAO = new ClientDAO();
    private Gson gson = new Gson();
    private SimplePromotionDAO simplePromotionDAO;


    public CaisseService() {}

    // What : envoie la liste des promo locale + formule
    // When : agenda (07:00)
    // Form : BO
    // To : Caisse (promotionsToCa)
    // Type : Message
    // Code : BO-CA-1
    public String promotionToCaisse() throws IOException {
        StructBO_CA_1DTO structBO_ca_1DTO = new StructBO_CA_1DTO();
        simplePromotionDAO = new SimplePromotionDAO();
        Collection<SimplePromotionEntity> simplePromotionEntities = simplePromotionDAO.findAll();
        DataBO_CA_1DTO dataBO_ca_1DTO = new DataBO_CA_1DTO();
        for (SimplePromotionEntity entity : simplePromotionEntities)
        {
            for (ProductEntity productEntity : entity.getProductEntitySet()) {
                PromotionBO_CA_1DTO promotionBO_ca_1DTO = new PromotionBO_CA_1DTO(productEntity.getRefProductRE(), entity.getUnite());
                dataBO_ca_1DTO.getList_promotion().add(promotionBO_ca_1DTO);
            }
        }
        structBO_ca_1DTO.setData(dataBO_ca_1DTO);
        structBO_ca_1DTO.setInstanceID(HomeController.instanceID);
        structBO_ca_1DTO.setSender(HomeController.appName);
        String result = "OK";

        if (HomeController.instanceID == 1) {
            result = sendController.sendMsg("CA", 1, "promotionsToCA", gson.toJson(structBO_ca_1DTO));
            result = sendController.sendMsg("CA", 2, "promotionsToCA", gson.toJson(structBO_ca_1DTO));
        }
        else
        {
            result = sendController.sendMsg("CA", 3, "promotionsToCA", gson.toJson(structBO_ca_1DTO));
            result = sendController.sendMsg("CA", 4, "promotionsToCA", gson.toJson(structBO_ca_1DTO));
        }


        return result;
    }

    // What : liste des produits
    // When : agneda (07:00)
    // Form : BO
    // To : Caisse (productsToCa)
    // Type : Message
    // Code : BO-CA-2
    public String produitToCaisse() throws IOException {
        DataBO_CA_2DTO dataToCA2DTO = new DataBO_CA_2DTO();
        Collection<ProductEntity> productEntities = productDAO.findAll();
        List<ProductBO_CA_2DTO> productBOCA2DTOList = new ArrayList<ProductBO_CA_2DTO>();
        for (ProductEntity entity : productEntities)
        {
            ProductBO_CA_2DTO item = new ProductBO_CA_2DTO(entity);
            productBOCA2DTOList.add(item);
        }
        dataToCA2DTO.setListProduct(productBOCA2DTOList);
        StructBO_CA_2DTO structResult = new StructBO_CA_2DTO(HomeController.appName, HomeController.instanceID, dataToCA2DTO);

        String result = "KO";

        System.out.println("try to send msg to CA");

        if (HomeController.instanceID == 1) {
            result = sendController.sendMsg("CA", 1, "productsToCA", gson.toJson(structResult));
            result = sendController.sendMsg("CA", 2, "productsToCA", gson.toJson(structResult));
        }
        else
        {
            result = sendController.sendMsg("CA", 3, "productsToCA", gson.toJson(structResult));
            result = sendController.sendMsg("CA", 4, "productsToCA", gson.toJson(structResult));
        }
        return result;
    }


    // What : le coupon de fidéleté
    // When : recu par Fi (clientToBack)
    // Form : clientToBack (Fi)
    // To : Caisse (couponToCa)
    // Type : Message
    // Code : BO-CA-3
    public String couponToCaisse(String data) throws IOException{
        return null;
    }


    // What : ticket de caisse
    // When : appel
    // Form : Caisse
    // To : ticketToFidelite
    // Type : Message
    // Code : CA-BO-1
    public String ticketToBO (String data) throws IOException {
        StructCA_BO_1DTO structCABo1DTO = gson.fromJson(data, StructCA_BO_1DTO.class);
        System.out.println("Parse Done " + structCABo1DTO.getSender());
        DataCA_BO_1DTO dataCABo1DTO = structCABo1DTO.getData();
        List<PairCA_BO_1> productEntities = new ArrayList<PairCA_BO_1>();
        boolean haveCommand = false;

        fideliteService.ticketToFidelite(structCABo1DTO);

        for (ProductCA_BO_1DTO productCABo1DTO : dataCABo1DTO.getProducts()) {
            System.out.println("get : " + productCABo1DTO.getProductRef());
            ProductEntity productEntity = productDAO.getByRef(productCABo1DTO.getProductRef());
            System.out.println("Product quantity : " + productEntity.getQuantite());

            if (productEntity.getQuantite() - productCABo1DTO.getQuantity() < 0) {
                haveCommand =  true;
                productEntities.add(new PairCA_BO_1(productEntity, productCABo1DTO.getQuantity(), true));
            } else {
                productEntities.add(new PairCA_BO_1(productEntity, productCABo1DTO.getQuantity(), false));
                productEntity.setQuantite(productEntity.getQuantite() - productCABo1DTO.getQuantity());
                productDAO.update(productEntity);
            }
        }

        ClientEntity clientEntity = null;
        try {
            clientEntity = clientDAO.getByRef(dataCABo1DTO.getCustomerID());
            System.out.println(dataCABo1DTO.getCustomerID() + "exist");
        } catch (Exception e){
            clientEntity = new ClientEntity();
            clientEntity.setRefClient(dataCABo1DTO.getCustomerID());
            clientDAO.persist(clientEntity);
            System.out.println(dataCABo1DTO.getCustomerID() + "created");
            //clientEntity = clientDAO.getByRef(dataCABo1DTO.getCustomerID());
        }

        if (haveCommand) {
            System.out.println("launch commandToCommercial");
            commercialService.commandToCommercial(clientEntity, productEntities, structCABo1DTO.getDate());
        }
        commercialService.addTicketToCommercial(productEntities, clientEntity);

        return "ok";
    }


}
