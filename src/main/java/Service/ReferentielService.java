package Service;

import com.google.gson.Gson;
import dao.ProductDAO;
import dto.referentiel.DataRE_BO_1DTO;
import dto.referentiel.ProductRE_BO_1DTO;
import dto.referentiel.StructRE_BO_1DTO;
import model.ProductEntity;

import java.io.IOException;

/**
 * Created by hugo on 07/01/17.
 */
public class ReferentielService {

    private ProductDAO productDao = new ProductDAO();

    private Gson gson = new Gson();

    // What : recoit les diff de referenciel
    // When : appel
    // Form : RE
    // To : BO
    // Type : File
    // Code : RE-BO-1
    public String produitToBO (String data) throws IOException { //List<ProductRE_BO_1DTO> productREBo1DTOList){
        System.out.println("ReferentielService : produitToBO");

        System.out.println(data);
        StructRE_BO_1DTO dataRE_bo_1DTO = gson.fromJson(data, StructRE_BO_1DTO.class);

        System.out.println(dataRE_bo_1DTO.getData().getList_product());

        for (ProductRE_BO_1DTO product: dataRE_bo_1DTO.getData().getList_product()) {
            ProductEntity productEntity = null;
            boolean isNew = false;
            try {
                productEntity = productDao.getByRef(product.getId_product_RE());
                System.out.println(product.getId_product_RE() + " : exist");
            } catch (Exception e){
                productEntity = new ProductEntity();
                System.out.println(product.getId_product_RE() + " : is new");
                isNew = true;
            }
            productEntity.setRefProductRE(product.getId_product_RE());
            productEntity.setRefProductFO(product.getId_product_FO());
            productEntity.setDescription(product.getDescription());
            productEntity.setPrice(product.getSale_price());
            productEntity.setFournisseur(product.getProvider_name());
            productEntity.setRef_product_fournisseur(product.getRef_provider());

            if (isNew){
                productEntity.setStockMax(20); // je ne sais pas
                productEntity.setStockMin(1); // je ne sais pas
                productEntity.setQuantite(0);
                productDao.persist(productEntity);
            } else {
                productDao.update(productEntity);
            }
        }
        System.out.println("produitToBO done");
        return "done";
    }
}
