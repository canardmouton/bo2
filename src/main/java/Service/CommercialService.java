package Service;

import Service.tools.FileService;
import Service.tools.MyDate;
import com.google.gson.Gson;
import controller.HomeController;
import controller.SendController;
import dao.CommandDAO;
import dao.CommandDetailDAO;
import dao.ProductDAO;
import dto.caisse.CA_BO_1.PairCA_BO_1;
import dto.gestionCommerciale.BO_GC_1.DataBO_GC_1DTO;
import dto.gestionCommerciale.BO_GC_1.ProductsBO_GC_1DTO;
import dto.gestionCommerciale.BO_GC_1.StructBO_GC_1DTO;
import dto.gestionCommerciale.BO_GC_2.DataBO_GC_2DTO;
import dto.gestionCommerciale.BO_GC_2.StructBO_GC_2DTO;
import dto.gestionCommerciale.BO_GC_3.DataBO_GC_3DTO;
import dto.gestionCommerciale.BO_GC_3.StructBO_GC_3DTO;
import dto.gestionCommerciale.BO_GC_4.DataBO_GC_4DTO;
import dto.gestionCommerciale.BO_GC_4.StructBO_GC_4DTO;
import dto.gestionCommerciale.BO_GC_5.DataBO_GC_5DTO;
import dto.gestionCommerciale.BO_GC_5.StructBO_GC_5DTO;
import dto.gestionCommerciale.GC_BO_1.ProductGC_BO_1DTO;
import dto.gestionCommerciale.GC_BO_1.StructGC_BO_1DTO;
import dto.gestionCommerciale.GC_BO_2.ProductGC_BO_2DTO;
import dto.gestionCommerciale.GC_BO_2.StructGC_BO_2DTO;
import model.ClientEntity;
import model.CommandDetailEntity;
import model.CommandEntity;
import model.ProductEntity;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

/**
 * Created by hugo on 07/01/17.
 */
public class CommercialService {

    private FileService fileService = new FileService();
    private SendController sendController = new SendController();
    private ProductDAO productDAO = new ProductDAO();
    private CommandDAO commandDAO = new CommandDAO();
    private CommandDetailDAO commandDetailDAO = new CommandDetailDAO();
    private Gson gson = new Gson();

    // What : envoie le bon de livraison  (perte avec les pertes)
    // When : recu par commandToBo
    // Form : CG
    // To : CG (livraisonToCommercial)
    // Type : File
    // Code : BO-GC-1
    public String livraisonToCommercial (DataBO_GC_1DTO data) throws IOException{
        System.out.println("commercial service livraisonToCommercial");
        StructBO_GC_1DTO structBO_gc_1DTO = new StructBO_GC_1DTO(HomeController.appName, HomeController.instanceID, data);
        String resutlString = gson.toJson(structBO_gc_1DTO);
        String filePath = fileService.createTmpFile("BO_GC_1", resutlString);
        System.out.println("livraison to commercial done");
        return sendController.sendFile(HomeController.gc.getNom(), HomeController.gc.getInstance(), "livraisonToCommercial", filePath);
    }

    // What : Validation et modification de la proposition de reassort
    // When : suit GC-BO-1
    // Form : GC
    // To : GC (reassortToCommercial)
    // Type : File
    // Code : BO-GC-2
    public String reassortToCommercial (StructGC_BO_1DTO structGC_bo_1DTO) throws IOException{
        System.out.println("CommercialService : reassortToCommercial");
        String refCommand = structGC_bo_1DTO.getData().getRef_command();
        System.out.println("ref command " + refCommand);
//        Date date = MyDate.fromString(dateString);
        CommandEntity commandEntity = new CommandEntity();
        System.out.println("commandEntity creat");
        commandEntity.setDateEnvoi(MyDate.fromString(structGC_bo_1DTO.getData().getDate()));
        System.out.println("commandEntity.setDate ok");
        commandEntity.setRefCommand(refCommand);
        commandDAO.persist(commandEntity);
        CommandEntity commandEntity1 = commandDAO.getByRef(refCommand);
        Set<CommandDetailEntity> commandDetailEntities = new HashSet<>();
        for (ProductGC_BO_1DTO product : structGC_bo_1DTO.getData().getListe_product()){

            CommandDetailEntity commandDetailEntity = new CommandDetailEntity();
            commandDetailEntity.setProduct(productDAO.getByRef(product.getRef_product()));
            commandDetailEntity.setQuantite(product.getQuantity());
            commandDetailEntity.setCommand(commandEntity1);
            commandDetailDAO.persist(commandDetailEntity);
            commandDetailEntities.add(commandDetailEntity);
        }

        StructBO_GC_2DTO structBO_gc_2DTO = new StructBO_GC_2DTO(HomeController.appName, HomeController.instanceID, new DataBO_GC_2DTO(structGC_bo_1DTO.getData()));

        String stringResult = gson.toJson(structBO_gc_2DTO);

        String pathFile = fileService.createTmpFile("BO_GC_2", stringResult);

        return sendController.sendFile(HomeController.gc.getNom(), HomeController.gc.getInstance(), "reassortToCommercial", pathFile);

    }

    // What : Envoie la commande client
    // When : commande (stock = 0 ticket caisse)
    // Form : caisse
    // To : GC (commandToCommercial)
    // Type : File
    // Code : BO-GC-3
    public String commandToCommercial (ClientEntity clientEntity, List<PairCA_BO_1> productEntities, String dateString) throws IOException{
        System.out.println("CommercialService : commandToCommercial");
        String refCommand = clientEntity.getRefClient() + "-" + dateString;
        System.out.println("ref command " + refCommand);
//        Date date = MyDate.fromString(dateString);
        CommandEntity commandEntity = new CommandEntity();
        System.out.println("commandEntity react");
        commandEntity.setClient(clientEntity);
        System.out.println("commandEntity. setclient ok");
        commandEntity.setDateEnvoi(MyDate.fromString(dateString));
        System.out.println("commandEntity.setDate ok");
        commandEntity.setRefCommand(refCommand);
        commandDAO.persist(commandEntity);
        CommandEntity commandEntity1 = commandDAO.getByRef(refCommand);
        Set<CommandDetailEntity> commandDetailEntities = new HashSet<>();
        for (PairCA_BO_1 pair : productEntities){
            if (pair.isCommand()){
                CommandDetailEntity commandDetailEntity = new CommandDetailEntity();
                commandDetailEntity.setProduct(pair.getProductEntity());
                commandDetailEntity.setQuantite(pair.getQuantity());
                commandDetailEntity.setCommand(commandEntity1);
                commandDetailDAO.persist(commandDetailEntity);
                commandDetailEntities.add(commandDetailEntity);

            }
        }
//        commandEntity.setCommandDetailEntities(commandDetailEntities);
//        commandDAO.persist(commandEntity);
        StructBO_GC_3DTO structBOGc3DTO = new StructBO_GC_3DTO();
        structBOGc3DTO.setSender(HomeController.appName);
        structBOGc3DTO.setInstanceID(HomeController.instanceID);
        structBOGc3DTO.setData(new DataBO_GC_3DTO(commandEntity1, commandDetailEntities));

        String result = "KO";
        String pathFile = "";
        //File tmpFile = null;
        try {
            pathFile = fileService.createTmpFile("BO-GC-3", gson.toJson(structBOGc3DTO));
            System.out.println("PathFile : " + pathFile);
//            tmpFile = File.createTempFile("BO-GC-3_", ".tmp");
//            FileOutputStream tmpOutputStream = new FileOutputStream(tmpFile);
//            tmpOutputStream.write(gson.toJson(result).getBytes());
//            tmpOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return sendController.sendFile(HomeController.gc.getNom(), HomeController.gc.getInstance(), "commandToCommercial", pathFile);
//
    }

    // What : Valide reception commande client
    // When : reception commande (GC-BO-2)
    // Form : GC
    // To : GC (retourLivraisonToCommercial)
    // Type : Message
    // Code : BO-GC-4
    public String retourLivraisonToCommercial (CommandEntity commandEntity) throws IOException{
        StructBO_GC_4DTO structBO_gc_4DTO = new StructBO_GC_4DTO();
        structBO_gc_4DTO.setSender(HomeController.appName);
        structBO_gc_4DTO.setInstanceID(HomeController.instanceID);
        DataBO_GC_4DTO dataBO_gc_4DTO = new DataBO_GC_4DTO(commandEntity.getRefCommand(), true);
        structBO_gc_4DTO.setData(dataBO_gc_4DTO);
        String result = gson.toJson(structBO_gc_4DTO);
        return sendController.sendMsg(HomeController.gc.getNom(), HomeController.gc.getInstance(), "retourLivraisonToCommercial", result);
    }


    // What : envoie les tickets de la journée
    // When : agenda (22:00)
    // Form : BO
    // To : GC (ticketToCommercial)
    // Type : File
    // Code : BO-GC-5
    public String ticketToCommercial (String data) throws IOException {
        if (HomeController.pathFileTickets != null)
            sendController.sendFile(HomeController.gc.getNom(), HomeController.gc.getInstance(), "ticketToCommercial", HomeController.pathFileTickets);
        HomeController.pathFileTickets = null;
        return "OK";
    }

    public String addTicketToCommercial(List<PairCA_BO_1> pairCA_bo_1List, ClientEntity clientEntity){
        if (HomeController.pathFileTickets == null){
            StructBO_GC_5DTO initStruc = new StructBO_GC_5DTO(HomeController.appName, HomeController.instanceID, null);
            System.out.println("initStruc : " + initStruc.getInstanceID());
            try {
                HomeController.pathFileTickets = fileService.createTmpFile("BO_GC_5", gson.toJson(initStruc));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        String initStringFromFile = "";
        try {
            initStringFromFile= fileService.fileToString(HomeController.pathFileTickets);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("initStringFromFile : " + initStringFromFile);
        StructBO_GC_5DTO structBOGc5DTO = gson.fromJson(initStringFromFile, StructBO_GC_5DTO.class);
        structBOGc5DTO.addData(new DataBO_GC_5DTO(pairCA_bo_1List, clientEntity));
        String result = "";
        try {
            result = fileService.createTmpFile("BO_GC_5", gson.toJson(structBOGc5DTO));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    // What : recoit bon de commande / verifie en fonction des stock / puis reassortToCommercial
    // When : recu
    // Form : GC
    // To : reassortToCommercial
    // Type : File
    // Code : GC-BO-1
    public String commandToBO (String data) throws IOException{
        System.out.println("CommercialService : commandToBO");
        String resultat = "KO";
        StructGC_BO_1DTO structGC_BO_1DTO = gson.fromJson(data, StructGC_BO_1DTO.class);
        System.out.println("fichier chargé=" + structGC_BO_1DTO.toString());
        ProductDAO productDAO = new ProductDAO();
        for (ProductGC_BO_1DTO productGCBo1DTO : structGC_BO_1DTO.getData().getListe_product()) {
            ProductEntity productEntity = productDAO.getByRef(productGCBo1DTO.getRef_product());
            int quantity = productEntity.getStockMax() - productEntity.getQuantite();
            productGCBo1DTO.setQuantity(quantity);
            System.out.println("Product quantity : " + productEntity.getQuantite());

        }

        resultat = this.reassortToCommercial(structGC_BO_1DTO);

        return resultat;

    }

    // What : recoit bon de livraison / increment le stock / simulation perte / livraisonToCommercial + si client retourLivraisonToCommercial
    // When : recu
    // Form : GC
    // To : livraisonToCommercial + retourLivraisonToCommercial
    // Type : File
    // Code : GC-BO-2
    public String livraisonToBO (String data) throws IOException{
        System.out.println("commercial service : livraisonToBO");
        StructGC_BO_2DTO structGC_bo_2DTO = gson.fromJson(data, StructGC_BO_2DTO.class);
        System.out.println("parsed struct");
        Random rand = new Random();

        boolean isCommandClient = false;
        CommandEntity commandEntity = null;
        try {
            commandEntity = commandDAO.getByRef(structGC_bo_2DTO.getData().getRef_command());
            ClientEntity clientEntity = commandEntity.getClient();
            if (clientEntity != null) {
                isCommandClient = true;
            }
            commandEntity.setDateReception(MyDate.fromString(structGC_bo_2DTO.getData().getDate()));
            System.out.println("set date ok");
            commandDAO.update(commandEntity);
            System.out.println("command entity update done");

        } catch (Exception e){
            System.out.println("command entity not found");
            return "KO";
        }

        DataBO_GC_1DTO dataBO_gc_1DTO = new DataBO_GC_1DTO();
        dataBO_gc_1DTO.setRef_delivery(structGC_bo_2DTO.getData().getRef_command());
        List<ProductGC_BO_2DTO> list = structGC_bo_2DTO.getData().getListe_products();
        if (list.isEmpty()) {
            System.out.println("list empty");
        }
        for (ProductGC_BO_2DTO productGC_bo_2DTO : structGC_bo_2DTO.getData().getListe_products()) {
            System.out.println("inside the for");
            ProductEntity entity = null;
            int pertes = rand.nextInt(10);
            if (pertes > 3) {
                pertes = 0;
            }
            if (pertes > productGC_bo_2DTO.getQuantity()) {
                pertes = productGC_bo_2DTO.getQuantity();
            }


            try {
                entity = productDAO.getByRef(productGC_bo_2DTO.getRef_product());
                dataBO_gc_1DTO.addProduct(new ProductsBO_GC_1DTO(productGC_bo_2DTO.getRef_product(), productGC_bo_2DTO.getQuantity() - pertes));
                if (!isCommandClient) {
                    entity.setQuantite(entity.getQuantite() + productGC_bo_2DTO.getQuantity() - pertes);
                    productDAO.update(entity);
                }
                else { //if it is a client order (si c'est une commande client)
                    this.retourLivraisonToCommercial(commandEntity);
                }


                try {
                    CommandDetailEntity commandDetailEntity = commandDetailDAO.getByProductCommand(entity, commandEntity);
                    commandDetailEntity.setPertes(pertes);
                    commandDetailDAO.update(commandDetailEntity);
                    System.out.println("updated commandDetailEntity");

                } catch(Exception e) {
                    System.out.println("commandDetailEntity doesn't exist");
                    e.printStackTrace();
                }
            } catch (Exception e){
                System.out.println("entity doesn't exist");

            }





        }
        return this.livraisonToCommercial(dataBO_gc_1DTO);


    }


}



