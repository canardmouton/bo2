package Service;

import com.google.gson.Gson;
import com.google.gson.JsonParser;
import controller.HomeController;
import dto.AbstractDTO;
import dto.ListProductBoUtDTO;
import dto.referentiel.DataRE_BO_1DTO;
import dto.referentiel.ProductRE_BO_1DTO;
import model.ProductEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

/**
 * Created by Steven on 02/11/2016.
 */
@Controller
public class ParseFunction {
    private String fct = "";
    //private Data data = new Data("", new Agenda[0]);
    private String data = "";
    private static final Logger LOG = LoggerFactory
            .getLogger(ParseFunction.class);

    private Gson gson = new Gson();


    private CaisseService caisseService = new CaisseService();
    private UserService userService = new UserService();
    private CommercialService commercialService = new CommercialService();
    private ReferentielService referentielService = new ReferentielService();
    private FideliteService fideliteService = new FideliteService();

    public ParseFunction(String fct, String data) {
        this.fct = fct;
        this.data = data;
    }

    public ParseFunction() {

    }

    public String getFct() {
        return fct;
    }

    public void setFct(String fct) {
        this.fct = fct;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }


    public String execute() {
        switch (fct) {
            //Flux sortant

            // What : envoie la liste des promo locale + formule
            // When : agenda (07:00)
            // Form : BO
            // To : Caisse (promotionsToCa)
            // Type : Message
            // Code : BO-CA-1
            case "promotionToCaisse": {
                System.out.println("ParseFunction : promotionToCaisse");
                String result = "OK";
                try {
                    result = caisseService.promotionToCaisse();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return result;
            }

            // What : liste des produits
            // When : agneda (07:00)
            // Form : BO
            // To : Caisse (productsToCa)
            // Type : Message
            // Code : BO-CA-2
            case "produitToCaisse": {
                System.out.println("ParseFunction : produitToCaisse");
                String result = "KO";
                try {
                    result = caisseService.produitToCaisse();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return result;
            }

            // What : le coupon de fidéleté
            // When : recu par Fi (clientToBack)
            // Form : clientToBack (Fi)
            // To : Caisse (couponToCa)
            // Type : Message
            // Code : BO-CA-3
            case "couponToCaisse": {
                System.out.println("ParseFunction : couponToCaisse");
                String result = "KO";
                try {
                    result = caisseService.couponToCaisse(data);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return result;
            }


            // What : envoie du ticket (isCommande)
            // When : ticket reçu de Caisse (CA-BO-1)
            // Form : Caisse
            // To : Fi
            // Type : Message
            // Code : BO-FI-1
            case "ticketToFidelite": {
                System.out.println("ParceFunction : ticketToFidelite");
                String result = "KO";
                try {
                    result = fideliteService.ticketToFidelite(null);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return result;
            }

            // What : envoie le bon de livraison  (perte avec les pertes)
            // When : recu par commandToBo
            // Form : CG
            // To : CG (livraisonToCommercial)
            // Type : File
            // Code : BO-GC-1
            case "livraisonToCommercial": {
                System.out.println("ParceFunction : livraisonToCommercial");
                String result = "KO";
                try {
                    result = commercialService.livraisonToCommercial(null);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return result;
            }

            // What : Validation et modification de la proposition de reassort
            // When : suit GC-BO-1
            // Form : GC
            // To : GC (reassortToCommercial)
            // Type : File
            // Code : BO-GC-2
            case "reassortToCommercial": {
                System.out.println("ParceFunction : reassortToCommercial");
                String result = "KO";
                try {
                    result = commercialService.reassortToCommercial(null);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return result;
            }

            // What : Envoie la commande client
            // When : commande (stock = 0 ticket caisse)
            // Form : caisse
            // To : GC (commandToCommercial)
            // Type : File
            // Code : BO-GC-3
            case "commandToCommercial": {
                System.out.println("ParceFunction : commandToCommercial");
                String result = "KO";
                try {
                    result = commercialService.commandToCommercial(null, null, null);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return result;
            }

            // What : Valide reception commande client
            // When : reception commande (GC-BO-2)
            // Form : GC
            // To : GC (retourLivraisonToCommercial)
            // Type : Message
            // Code : BO-GC-4
            case "retourLivraisonToCommercial": {
                System.out.println("ParceFunction : retourLivraisonToCommercial");
                String result = "KO";
                try {
                    result = commercialService.retourLivraisonToCommercial(null);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return result;
            }

            // What : envoie les tickets de la journée
            // When : agenda (22:00)
            // Form : BO
            // To : GC (ticketToCommercial)
            // Type : File
            // Code : BO-GC-5
            case "ticketToCommercial": {
                System.out.println("ParceFunction : ticketToCommercial");
                String result = "KO";
                try {
                    result = commercialService.ticketToCommercial(data);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return result;
            }

            // What : retourne l'inventaire produit
            // When : appel webService
            // From : BO
            // To : Utilisateur
            // Type : WebService
            // Code : BO-UT-1
            case "inventaireToUser": {
                System.out.println("ParseFunction : inventaireToUser");
                try {
                    AbstractDTO<ListProductBoUtDTO> abstractDTO = new AbstractDTO<ListProductBoUtDTO>();
                    ListProductBoUtDTO listProductBoUtDTO = userService.inventaireToUser(data);
                    abstractDTO.setSender(HomeController.appName);
                    abstractDTO.setInstanceID(HomeController.instanceID);
                    abstractDTO.setData(listProductBoUtDTO);
                    String result = gson.toJson(abstractDTO);
                    System.out.println("result");
                    System.out.println(result);
                    return result;
                } catch (IOException e) {
                    e.printStackTrace();
                }


                return "KO";
            }

            //Flux entrant

            // What : recoit les diff de referenciel
            // When : appel
            // Form : RE
            // To : BO
            // Type : File
            // Code : RE-BO-1
            case "productToBO": {
                System.out.println("ParseFunction : productToBO");

                String result = "KO";
                try {
                    result = referentielService.produitToBO(data);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return result;
            }

            // What : ticket de caisse
            // When : appel
            // Form : Caisse
            // To : ticketToFidelite
            // Type : Message
            // Code : CA-BO-1
            case "ticketToBO": {
                System.out.println("ParseFunction : ticketToBO");

                String result = "KO";
                try {
                    result = caisseService.ticketToBO(data);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return result;
            }

            // What : recoit bon de commande / verifie en fonction des stock / puis reassortToCommercial
            // When : recu
            // Form : GC
            // To : reassortToCommercial
            // Type : File
            // Code : GC-BO-1
            case "commandToBO": {
                System.out.println("ParseFunction : commandToBO");
//                JSONObject jsonObj = new JSONObject(data);
//                String file = jsonObj.get("fileID").toString().replaceAll("\"", "")
//                        .replaceAll("[\\[\\]\"]", "").replaceAll(" ", "+");
//
//                String downloadFileUrl = "http://" + HomeController.stip + "/api/get_file?target=" + HomeController.appName
//                        + "&targetInstance=" + HomeController.instanceID + "&fileID=" + file;
//
//                MyHttpGetFile myHttpGetFile = new MyHttpGetFile(downloadFileUrl, "/project/upload-dir/", file,
//                        Integer.toString(HomeController.instanceID));
//
//                System.out.println("file downloaded");
//
//                String targetFilePath = "http://" + HomeController.stip + "/api/get_file?target=" + HomeController.appName
//                        + "&targetInstance=" + HomeController.instanceID + "&fileID=" + file;
//                System.out.println("Please download with this link : " + targetFilePath);
//
//                String resultat = "KO";
//                try { // On va charger le fichier telecharger
//                    String result = myHttpGetFile.execute(); //
//
//                    JsonParser parser = new JsonParser();
//                    try {
//                        String filepath = "/project/upload-dir/" + file;
//                        Object fileObject = parser.parse(new FileReader(filepath));
//                        StructGC_BO_1DTO structGC_BO_1DTO = new Gson().fromJson(parser.parse(fileObject.toString()), StructGC_BO_1DTO.class);
//
//                        System.out.println("fichier chargé=" + structGC_BO_1DTO.toString());
//                        ProductDAO productDAO = new ProductDAO();
//                        for (ProductGC_BO_1DTO productGCBo1DTO : structGC_BO_1DTO.getData().getListProduct()) {
//                            ProductEntity productEntity = productDAO.getByRef(productGCBo1DTO.getRefProduct());
//                            int quantity = productEntity.getStockMax() - productEntity.getQuantite();
//                            productGCBo1DTO.setQuantity(quantity);
//                            System.out.println("Product quantity : " + productEntity.getQuantite());
//
//                        }
//
//                        resultat = commercialService.reassortToCommercial(structGC_BO_1DTO);
//                    }
//                    catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }



                String result = "KO";

                try {

                    result = commercialService.commandToBO(data);
                } catch (IOException e) {
                    e.printStackTrace();
                }


                return result;
            }


            // What : recoit bon de livraison / increment le stock / simulation perte / livraisonToCommercial + si client retourLivraisonToCommercial
            // When : recu
            // Form : GC
            // To : livraisonToCommercial + retourLivraisonToCommercial
            // Type : File
            // Code : GC-BO-2
            case "livraisonToBO": {
                System.out.println("ParseFunction : livraisonToBO");
                String result = "KO";
                try {
                    result = commercialService.livraisonToBO(data);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return result;
            }

            // What : coupon de fidelite
            // When : appel
            // Form : FI
            // To : couponToCaisse
            // Type : Message
            // Code : FI-BO-1
            case "clientToBack":{
                System.out.println("ParceFunction : clientToBack");
                String result = "KO";
                try {
                    result = fideliteService.clientToBack(data);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return result;
            }

            // What : catalogue de coupons de fidelite à transmettre a la caisse (fct ?)
            // When : appel
            // Form : FI
            // To : caisse
            // Type : Message
            // Code : FI-BO-2
            case "catalogToBack":{
                System.out.println("ParseFunction : catalogToBack");
                String result = "KO";
                try {
                    result = fideliteService.catalogToBack(data);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return result;
            }

            case "test": {
                System.out.println("ParceFunction : test");
                System.out.println(data);
                return "ok";
            }


            default: {
                System.out.println("Unknown function: " + fct);
                break;
            }
        }

        return new Gson().toJson("msg");
    }


    /*private void loadAndPrintFile(String filepath) {
        System.out.println("loadAndPrintFile = " +  filepath);

        JsonParser parser = new JsonParser();
        try {
            Object fileObject = parser.parse(new FileReader(filepath));
            DataRE_BO_1DTO dataRE_bo_1DTO = new Gson().fromJson(parser.parse(fileObject.toString()), DataRE_BO_1DTO.class);

            List<ProductRE_BO_1DTO> productREBo1DTOList = dataRE_bo_1DTO.getList_product();

            EntityManagerFactory emf;
            EntityManager em;
            emf = Persistence.createEntityManagerFactory("bo");
            em = emf.createEntityManager();

            em.getTransaction().begin();
            for (ProductRE_BO_1DTO product: productREBo1DTOList) {
                ProductEntity productEntity = new ProductEntity(
                        product.getId_product_RE(),
                        product.getId_product_FO(),
                        product.getRef_provider(),
                        2,
                        product.getSale_price(),
                        2,
                        10,
                        product.getProvider_name(),
                        product.getDescription()
                );
                em.persist(productEntity);
            }
            em.getTransaction().commit();

            System.out.println("end of insertion");

            System.out.println("object = " + dataRE_bo_1DTO.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }*/
}
