package model;

import javax.persistence.*;

/**
 * Created by supercanard on 07/01/17.
 */
@Entity
@Table(name = "command_detail", schema = "public", catalog = "bo2")
public class CommandDetailEntity {
    private int commandDetailId;
    private int quantite;
    private int pertes;
    private CommandEntity command;
    private ProductEntity product;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @SequenceGenerator(name="command_detail_command_detail_id_seq", sequenceName="command_detail_command_detail_id_seq", allocationSize=1)
    @Column(name = "command_detail_id", nullable = false, insertable = true, updatable = true)
    public int getCommandDetailId() {
        return commandDetailId;
    }

    public void setCommandDetailId(int commandDetailId) {
        this.commandDetailId = commandDetailId;
    }

    @Basic
    @Column(name = "quantite", nullable = false, insertable = true, updatable = true)
    public int getQuantite() {
        return quantite;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }

    @Basic
    @Column(name = "pertes", nullable = false, insertable = true, updatable = true)
    public int getPertes() {
        return pertes;
    }

    public void setPertes(int pertes) {
        this.pertes = pertes;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ref_command_id")
    public CommandEntity getCommand()
    {
        return this.command;
    }

    public void setCommand(CommandEntity commandEntity)
    {
        this.command = commandEntity;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ref_product_id")
    public ProductEntity getProduct()
    {
        return this.product;
    }

    public void setProduct(ProductEntity productEntity)
    {
        this.product = productEntity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CommandDetailEntity that = (CommandDetailEntity) o;

        if (commandDetailId != that.commandDetailId) return false;
        if (quantite != that.quantite) return false;
        if (pertes != that.pertes) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = commandDetailId;
        result = 31 * result + quantite;
        result = 31 * result + pertes;
        return result;
    }

  }
