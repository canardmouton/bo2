package model;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by supercanard on 07/01/17.
 */
@Entity
@Table(name = "rule", schema = "public", catalog = "bo2")
public class RuleEntity {
    private int ruleId;
    private int montant;
    private Integer resultat;
    private String unit;
    private Set<LocalPromotionEntity> localPromotionEntities;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @SequenceGenerator(name="rule_rule_id_seq", sequenceName="rule_rule_id_seq", allocationSize=1)
    @Column(name = "rule_id", nullable = false, insertable = true, updatable = true)
    public int getRuleId() {
        return ruleId;
    }

    public void setRuleId(int ruleId) {
        this.ruleId = ruleId;
    }

    @Basic
    @Column(name = "montant", nullable = false, insertable = true, updatable = true)
    public int getMontant() {
        return montant;
    }

    public void setMontant(int montant) {
        this.montant = montant;
    }

    @Basic
    @Column(name = "resultat", nullable = true, insertable = true, updatable = true)
    public Integer getResultat() {
        return resultat;
    }

    public void setResultat(Integer resultat) {
        this.resultat = resultat;
    }

    @Basic
    @Column(name = "unit", nullable = false, insertable = true, updatable = true, length = 2147483647)
    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    @OneToMany(mappedBy = "rule", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    public Set<LocalPromotionEntity> getLocalPromotionEntitiesSet() {return localPromotionEntities;}

    public void setLocalPromotionEntitiesSet(Set<LocalPromotionEntity> localPromotionEntities) {this.localPromotionEntities = localPromotionEntities;}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RuleEntity that = (RuleEntity) o;

        if (ruleId != that.ruleId) return false;
        if (montant != that.montant) return false;
        if (resultat != null ? !resultat.equals(that.resultat) : that.resultat != null) return false;
        if (unit != null ? !unit.equals(that.unit) : that.unit != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = ruleId;
        result = 31 * result + montant;
        result = 31 * result + (resultat != null ? resultat.hashCode() : 0);
        result = 31 * result + (unit != null ? unit.hashCode() : 0);
        return result;
    }
}
