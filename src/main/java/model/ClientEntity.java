package model;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by supercanard on 07/01/17.
 */
@Entity
@Table(name = "client", schema = "public", catalog = "bo2")
public class ClientEntity {
    private int clientId;
    private String refClient;
    private Set<CommandEntity> commandEntitySet;


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @SequenceGenerator(name="client_client_id_seq", sequenceName="client_client_id_seq", allocationSize=1)
    @Column(name = "client_id", nullable = false, insertable = true, updatable = true)
    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    @Basic
    @Column(name = "ref_client", nullable = false, insertable = true, updatable = true, length = 64)
    public String getRefClient() {
        return refClient;
    }

    public void setRefClient(String adress) {
        this.refClient = adress;
    }

    @OneToMany(mappedBy = "client", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    public Set<CommandEntity> getCommandEntitySet() {return commandEntitySet;}

    public void setCommandEntitySet(Set<CommandEntity> commandEntitySet) {this.commandEntitySet = commandEntitySet;}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ClientEntity that = (ClientEntity) o;

        if (clientId != that.clientId) return false;
        if (refClient != null ? !refClient.equals(that.refClient) : that.refClient != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = clientId;
        result = 31 * result + (refClient != null ? refClient.hashCode() : 0);
        return result;
    }
}
