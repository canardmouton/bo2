package model;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

/**
 * Created by supercanard on 07/01/17.
 */
@Entity
@Table(name = "command", schema = "public", catalog = "bo2")
public class CommandEntity {
    private int commandId;
    private String refCommand;
    private Date dateEnvoi;
    private Date dateReception;
    private ClientEntity client;
    private Set<CommandDetailEntity> commandDetailEntities;


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @SequenceGenerator(name="command_command_id_seq", sequenceName="command_command_id_seq", allocationSize=1)
    @Column(name = "command_id", nullable = false, insertable = true, updatable = true)
    public int getCommandId() {
        return commandId;
    }

    public void setCommandId(int commandId) {
        this.commandId = commandId;
    }

    @Basic
    @Column(name = "ref_command", nullable = false, insertable = true, updatable = true, length = 32)
    public String getRefCommand() {
        return refCommand;
    }

    public void setRefCommand(String refCommand) {
        this.refCommand = refCommand;
    }

    @Basic
    @Column(name = "date_envoi", nullable = false, insertable = true, updatable = true)
    public Date getDateEnvoi() {
        return dateEnvoi;
    }

    public void setDateEnvoi(Date dateEnvoi) {
        this.dateEnvoi = dateEnvoi;
    }

    @Basic
    @Column(name = "date_reception", nullable = true, insertable = true, updatable = true)
    public Date getDateReception() {
        return dateReception;
    }

    public void setDateReception(Date dateReception) {
        this.dateReception = dateReception;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="ref_client_id")
    public ClientEntity getClient() {return client;}

    public void setClient(ClientEntity clientEntity) {this.client = clientEntity;}

    @OneToMany(mappedBy = "command", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    public Set<CommandDetailEntity> getCommandDetailEntities()
    {
        return commandDetailEntities;
    }

    public void setCommandDetailEntities(Set<CommandDetailEntity> commandDetailEntities)
    {
        this.commandDetailEntities = commandDetailEntities;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CommandEntity that = (CommandEntity) o;

        if (commandId != that.commandId) return false;
        if (refCommand != null ? !refCommand.equals(that.refCommand) : that.refCommand != null) return false;
        if (dateEnvoi != null ? !dateEnvoi.equals(that.dateEnvoi) : that.dateEnvoi != null) return false;
        if (dateReception != null ? !dateReception.equals(that.dateReception) : that.dateReception != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = commandId;
        result = 31 * result + (refCommand != null ? refCommand.hashCode() : 0);
        result = 31 * result + (dateEnvoi != null ? dateEnvoi.hashCode() : 0);
        result = 31 * result + (dateReception != null ? dateReception.hashCode() : 0);
        return result;
    }

  }
