package model;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by supercanard on 07/01/17.
 */
@Entity
@Table(name = "local_promotion", schema = "public", catalog = "bo2")
public class LocalPromotionEntity {
    private int localPromotionId;
    private String refLocalPromotion;
    private String designation;
    private boolean recurence;
    private RuleEntity rule;
    private Set<ProductPromotionEntity> productPromotionEntities;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="ref_id_rule")
    public RuleEntity getRule() {
        return rule;
    }

    public void setRule(RuleEntity ruleEntity) {
        this.rule = ruleEntity;
    }

    @OneToMany(mappedBy = "localPromotion", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    public Set<ProductPromotionEntity> getProductPromotionEntities()
    {
        return productPromotionEntities;
    }

    public void setProductPromotionEntities(Set<ProductPromotionEntity> productPromotionEntities)
    {
        this.productPromotionEntities = productPromotionEntities;
    }


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @SequenceGenerator(name="local_promotion_local_promotion_id_seq", sequenceName="local_promotion_local_promotion_client_id_seq", allocationSize=1)
    @Column(name = "local_promotion_id", nullable = false, insertable = true, updatable = true)
    public int getLocalPromotionId() {
        return localPromotionId;
    }

    public void setLocalPromotionId(int localPromotionId) {
        this.localPromotionId = localPromotionId;
    }

    @Basic
    @Column(name = "ref_local_promotion", nullable = false, insertable = true, updatable = true, length = 32)
    public String getRefLocalPromotion() {
        return refLocalPromotion;
    }

    public void setRefLocalPromotion(String refLocalPromotion) {
        this.refLocalPromotion = refLocalPromotion;
    }

    @Basic
    @Column(name = "designation", nullable = false, insertable = true, updatable = true, length = 64)
    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    @Basic
    @Column(name = "recurence", nullable = false, insertable = true, updatable = true)
    public boolean isRecurence() {
        return recurence;
    }

    public void setRecurence(boolean recurence) {
        this.recurence = recurence;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LocalPromotionEntity that = (LocalPromotionEntity) o;

        if (localPromotionId != that.localPromotionId) return false;
        if (recurence != that.recurence) return false;
        if (refLocalPromotion != null ? !refLocalPromotion.equals(that.refLocalPromotion) : that.refLocalPromotion != null)
            return false;
        if (designation != null ? !designation.equals(that.designation) : that.designation != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = localPromotionId;
        result = 31 * result + (refLocalPromotion != null ? refLocalPromotion.hashCode() : 0);
        result = 31 * result + (designation != null ? designation.hashCode() : 0);
        result = 31 * result + (recurence ? 1 : 0);
        return result;
    }
}
