package model;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

/**
 * Created by supercanard on 27/01/17.
 */

@Entity
@Table(name = "simple_promotion", schema = "public", catalog = "bo2")
public class SimplePromotionEntity {
    private int simplePromotionId;
    private Date dateStart;
    private Date dateStop;
    private float unite;
    private Set<ProductEntity> productEntitySet;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @SequenceGenerator(name="simple_promotion_simple_promotion_id_seq", sequenceName="simple_promotion_simple_promotion_id_seq", allocationSize=1)
    @Column(name = "simple_promotion_id", nullable = false, insertable = true, updatable = true)
    public int getSimplePromotionId() {
        return simplePromotionId;
    }

    public void setSimplePromotionId(int simplePromotionId) {
        this.simplePromotionId = simplePromotionId;
    }

    @Basic
    @Column(name = "date_start", nullable = false, insertable = true, updatable = true)
    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    @Basic
    @Column(name = "date_stop", nullable = false, insertable = true, updatable = true)
    public Date getDateStop() {
        return dateStop;
    }

    public void setDateStop(Date dateStop) {
        this.dateStop = dateStop;
    }

    @Basic
    @Column(name = "unite", nullable = false, insertable = true, updatable = true)
    public float getUnite() {
        return unite;
    }

    public void setUnite(float unite) {
        this.unite = unite;
    }

    @OneToMany(mappedBy = "simplePromotionEntity", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    public Set<ProductEntity> getProductEntitySet() {
        return productEntitySet;
    }

    public void setProductEntitySet(Set<ProductEntity> product) {
        this.productEntitySet = product;
    }
}
