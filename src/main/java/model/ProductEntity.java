package model;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by supercanard on 07/01/17.
 */
@Entity
@Table(name = "product", schema = "public", catalog = "bo2")
public class ProductEntity {
    private int productId;
    private String refProductRE; // idProductRE
    private String refProductFO; // idproductFOprivate String refProduct; // idProduct
    private int quantite;
    private double price; // salePrice
    private int stockMin;
    private int stockMax;
    private String fournisseur; // provider Name
    private String description;
    private Set<ProductPromotionEntity> productPromotionEntities;
    private Set<CommandDetailEntity> commandDetailEntities;
    private SimplePromotionEntity simplePromotionEntity;
    private int refProductFournisseur;

    public ProductEntity(String refProductRE, String refProductFO, int ref_product_fournisseur, int quantite, double price,
                         int stockMin, int stockMax, String fournisseur, String description) {
        this.refProductRE = refProductRE;
        this.refProductFO = refProductFO;
        this.refProductFournisseur = ref_product_fournisseur;
        this.quantite = quantite;
        this.price = price;
        this.stockMin = stockMin;
        this.stockMax = stockMax;
        this.fournisseur = fournisseur;
        this.description = description;
    }

    public ProductEntity() { }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @SequenceGenerator(name="product_product_id_seq", sequenceName="product_product_id_seq", allocationSize=1)
    @Column(name = "product_id", nullable = false, insertable = true, updatable = true)
    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    @Basic
    @Column(name = "ref_product_re", nullable = false, insertable = true, updatable = true, length = 32)
    public String getRefProductRE() {
        return refProductRE;
    }

    public void setRefProductRE(String refProductRE) {
        this.refProductRE = refProductRE;
    }

    @Basic
    @Column(name = "ref_product_fo", nullable = false, insertable = true, updatable = true, length = 32)
    public String getRefProductFO() {
        return refProductFO;
    }

    public void setRefProductFO(String refProductFO) {
        this.refProductFO = refProductFO;
    }

    @Basic
    @Column(name = "ref_product_fournisseur", nullable = false, insertable = true, updatable = true)
    public int getRef_product_fournisseur() {
        return refProductFournisseur;
    }

    public void setRef_product_fournisseur(int ref_product_fournisseur) {
        this.refProductFournisseur = ref_product_fournisseur;
    }

    @Basic
    @Column(name = "quantite", nullable = false, insertable = true, updatable = true)
    public int getQuantite() {
        return quantite;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }

    @Basic
    @Column(name = "price", nullable = false, insertable = true, updatable = true)
    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Basic
    @Column(name = "stock_min", nullable = false, insertable = true, updatable = true)
    public int getStockMin() {
        return stockMin;
    }

    public void setStockMin(int stockMin) {
        this.stockMin = stockMin;
    }

    @Basic
    @Column(name = "stock_max", nullable = false, insertable = true, updatable = true)
    public int getStockMax() {
        return stockMax;
    }

    public void setStockMax(int stockMax) {
        this.stockMax = stockMax;
    }

    @Basic
    @Column(name = "fournisseur", nullable = false, insertable = true, updatable = true)
    public String getFournisseur()
    {
        return this.fournisseur;
    }

    public void setFournisseur(String fournisseur)
    {
        this.fournisseur = fournisseur;
    }

    @Basic
    @Column(name = "description", nullable = false, insertable = true, updatable = true)
    public String getDescription()
    {
        return this.description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    public Set<ProductPromotionEntity> getProductPromotionEntities()
    {
        return productPromotionEntities;
    }

    public void setProductPromotionEntities(Set<ProductPromotionEntity> productPromotionEntities)
    {
        this.productPromotionEntities = productPromotionEntities;
    }

    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    public Set<CommandDetailEntity> getCommandDetailEntities()
    {
        return commandDetailEntities;
    }

    public void setCommandDetailEntities(Set<CommandDetailEntity> commandDetailEntities)
    {
        this.commandDetailEntities = commandDetailEntities;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="ref_simple_promotion_id")
    public SimplePromotionEntity getSimplePromotionEntity() {
        return simplePromotionEntity;
    }

    public void setSimplePromotionEntity(SimplePromotionEntity simplePromotionEntity) {
        this.simplePromotionEntity = simplePromotionEntity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProductEntity that = (ProductEntity) o;

        if (productId != that.productId) return false;
        if (quantite != that.quantite) return false;
        if (Double.compare(that.price, price) != 0) return false;
        if (stockMin != that.stockMin) return false;
        if (stockMax != that.stockMax) return false;
        if (refProductFournisseur != that.refProductFournisseur) return false;
        if (fournisseur != null ? !fournisseur.equals(that.fournisseur) : that.fournisseur != null) return false;
        if (description != null ? !description.equals(that.description) : that.description != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = productId;
        result = 31 * result + (refProductRE != null ? refProductRE.hashCode() : 0);
        result = 31 * result + (refProductFO != null ? refProductFO.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + quantite;
        temp = Double.doubleToLongBits(price);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + stockMin;
        result = 31 * result + stockMax;
        result = 31 * result + refProductFournisseur;
        result = 31 * result + (fournisseur != null ? fournisseur.hashCode() : 0);
        return result;
    }
}
