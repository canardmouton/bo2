package model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by supercanard on 07/01/17.
 */
@Entity
@Table(name = "product_promotion", schema = "public", catalog = "bo2")
public class ProductPromotionEntity {
    private int productPromotionId;
    private Date dateStart;
    private Date dateStop;
    private ProductEntity product;
    private LocalPromotionEntity localPromotion;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @SequenceGenerator(name="product_promotion_product_promotion_id_seq", sequenceName="product_promotion_product_promotion_client_id_seq", allocationSize=1)
    @Column(name = "product_promotion_id", nullable = false, insertable = true, updatable = true)
    public int getProductPromotionId() {
        return productPromotionId;
    }

    public void setProductPromotionId(int productPromotionId) {
        this.productPromotionId = productPromotionId;
    }

    @Basic
    @Column(name = "date_start", nullable = false, insertable = true, updatable = true)
    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    @Basic
    @Column(name = "date_stop", nullable = false, insertable = true, updatable = true)
    public Date getDateStop() {
        return dateStop;
    }

    public void setDateStop(Date dateStop) {
        this.dateStop = dateStop;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ref_id_product")
    public ProductEntity getProduct(){
        return this.product;
    }

    public void setProduct(ProductEntity productEntity){
        this.product = productEntity;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ref_id_promotion")
    public LocalPromotionEntity getLocalPromotion()
    {
        return localPromotion;
    }

    public void setLocalPromotion(LocalPromotionEntity localPromotion)
    {
        this.localPromotion = localPromotion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProductPromotionEntity that = (ProductPromotionEntity) o;

        if (productPromotionId != that.productPromotionId) return false;
        if (dateStart != null ? !dateStart.equals(that.dateStart) : that.dateStart != null) return false;
        if (dateStop != null ? !dateStop.equals(that.dateStop) : that.dateStop != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = productPromotionId;
        result = 31 * result + (dateStart != null ? dateStart.hashCode() : 0);
        result = 31 * result + (dateStop != null ? dateStop.hashCode() : 0);
        return result;
    }

   }
