package env;

/**
 * Created by claebo_c on 21/01/17.
 */
public class EnvEntity {
    private String nom;
    private int instance;

    public EnvEntity(String nom, int instance) {
        this.nom = nom;
        this.instance = instance;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getInstance() {
        return instance;
    }

    public void setInstance(int instance) {
        this.instance = instance;
    }



}
