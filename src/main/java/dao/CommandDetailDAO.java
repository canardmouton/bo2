package dao;

import model.CommandDetailEntity;
import model.CommandEntity;
import model.ProductEntity;

import javax.persistence.Query;

/**
 * Created by claebo_c on 22/01/17.
 */
public class CommandDetailDAO extends AbstractJpaDAO<CommandDetailEntity, Integer> {

    public CommandDetailEntity getByProductCommand(ProductEntity product, CommandEntity command) {
        System.out.println("getByProductCommand " + entityName + " product = " + product.getRefProductRE() + " command =" + command.getRefCommand());

        if (em == null)
            System.out.println("entitymanager NULL");

        Query query = em.createQuery("select E from " + entityName + " E where ref_command_id = :refc AND ref_product_id = :refp" );
        query.setParameter("refc", command.getCommandId());
        query.setParameter("refp", product.getProductId());
        if (query == null)
            System.out.println("Query NULL");

        return (CommandDetailEntity) query.getSingleResult();
    }

}
