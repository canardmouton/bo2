package dao;

import model.CommandEntity;
import model.ProductEntity;

import javax.persistence.Query;

/**
 * Created by supercanard on 20/01/17.
 */
public class CommandDAO extends AbstractJpaDAO<CommandEntity, Integer> {
    public CommandEntity getByRef(String ref) {
        System.out.println("getByRef " + entityName + " ref = " + ref);

        if (em == null)
            System.out.println("entitymanager NULL");

        Query query = em.createQuery("select E from " + entityName + " E where ref_command = :ref " );
        query.setParameter("ref", ref);
        if (query == null)
            System.out.println("Query NULL");

        return (CommandEntity) query.getSingleResult();
    }
}
