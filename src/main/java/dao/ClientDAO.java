package dao;

import model.ClientEntity;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import util.HibernateUtil;

import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by supercanard on 07/01/17.
 */
public class ClientDAO extends AbstractJpaDAO<ClientEntity, Integer>{

    public ClientEntity getByRef(String ref) {
        System.out.println("getByRef " + entityName);

        if (em == null)
            System.out.println("entitymanager NULL");

        Query query = em.createQuery("select E from " + entityName + " E where ref_client = :ref " );
        query.setParameter("ref", ref);
        if (query == null)
            System.out.println("Query NULL");

        return (ClientEntity) query.getSingleResult();
    }


}
