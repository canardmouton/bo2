package dao;

import model.ProductEntity;

import javax.persistence.Query;

/**
 * Created by supercanard on 14/01/17.
 */
public class ProductDAO extends AbstractJpaDAO<ProductEntity, Integer> {

    public ProductEntity getByRef(String ref) {
        System.out.println("getByRef " + entityName + " ref = " + ref);

        if (em == null)
            System.out.println("entitymanager NULL");

        Query query = em.createQuery("select E from " + entityName + " E where ref_product_re = :ref " );
        query.setParameter("ref", ref);
        if (query == null)
            System.out.println("Query NULL");

        return (ProductEntity) query.getSingleResult();
    }
}
