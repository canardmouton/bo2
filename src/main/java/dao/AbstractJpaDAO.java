package dao;

import javax.persistence.*;
import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.util.Collection;

/**
 * Created by claebo_c on 14/01/17.
 */
@SuppressWarnings({"unchecked"})
public abstract class AbstractJpaDAO<E, ID extends Serializable> {

    protected Class<E> entityClass;
    protected String entityName = "";

    protected EntityManager em;


    public AbstractJpaDAO() {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("bo2");
        this.em = emf.createEntityManager();

        this.entityClass = (Class<E>) ((ParameterizedType) getClass()
                .getGenericSuperclass()).getActualTypeArguments()[0];

        Annotation[] annotations = entityClass.getAnnotations();
        for(Annotation annotation : annotations) {
            if(annotation instanceof Entity) {
                entityName = ((Entity) annotation).name();
                break;
            }
        }

        if(entityName.equals("")) {
            entityName = entityClass.getSimpleName();
        }
    }

    public void setEntityManager(EntityManager em) {
        this.em = em;
    }

    protected EntityManager getEntityManager() {
        return em;
    }

    public E getById(ID id) {
        return em.find(entityClass, id);
    }

//    public E getByRef(String ref)
//    {
//        return em.find(entityClass, ref);
//    }

    public void persist(E entity) {
        em.getTransaction().begin();
        em.persist(entity);
        em.getTransaction().commit();
    }

    public void update(E entity) {
        em.getTransaction().begin();
        em.merge(entity);
        em.getTransaction().commit();
    }

    public void remove(E entity) {
        em.getTransaction().begin();
        em.remove(entity);
        em.getTransaction().commit();
    }

    public Collection<E> findAll() {
        System.out.println("findAll " + entityName);

        if (em == null)
            System.out.println("entitymanager NULL");

        Query query = em.createQuery("select E from " + entityName + " E where 1 = 1");

        if (query == null)
            System.out.println("Query NULL");

        return query.getResultList();
    }
}