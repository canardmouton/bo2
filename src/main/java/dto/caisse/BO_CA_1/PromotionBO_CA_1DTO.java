package dto.caisse.BO_CA_1;

/**
 * Created by hugo on 23/01/17.
 */
public class PromotionBO_CA_1DTO {
    private String productRef;
    private float valeur;

    public PromotionBO_CA_1DTO(String productRef, float valeur) {
        this.productRef = productRef;
        this.valeur = valeur;
    }

    public String getProductRef() {
        return productRef;
    }

    public void setProductRef(String productRef) {
        this.productRef = productRef;
    }

    public float getValeur() {
        return valeur;
    }

    public void setValeur(float valeur) {
        this.valeur = valeur;
    }
}
