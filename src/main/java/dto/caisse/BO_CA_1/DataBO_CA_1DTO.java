package dto.caisse.BO_CA_1;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hugo on 23/01/17.
 */
public class DataBO_CA_1DTO {
    private List<PromotionBO_CA_1DTO> list_promotion= new ArrayList<PromotionBO_CA_1DTO>();

    public DataBO_CA_1DTO() {
    }

    public DataBO_CA_1DTO(List<PromotionBO_CA_1DTO> list_promotion) {
        this.list_promotion = list_promotion;
    }

    public List<PromotionBO_CA_1DTO> getList_promotion() {
        return list_promotion;
    }

    public void setList_promotion(List<PromotionBO_CA_1DTO> list_promotion) {
        this.list_promotion = list_promotion;
    }
}
