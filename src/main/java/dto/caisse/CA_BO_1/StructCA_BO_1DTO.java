package dto.caisse.CA_BO_1;

/**
 * Created by hugo on 20/01/17.
 */
public class StructCA_BO_1DTO {
    //p1rivate SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    private String date;
    private int instanceID;
    private int yourInstance = 0;
    private String sender;
    private String yourApp = "";
    private DataCA_BO_1DTO data = new DataCA_BO_1DTO();

    public StructCA_BO_1DTO() {
    }

    public StructCA_BO_1DTO(String date, int instanceID, int yourInstance, String sender, String yourApp, DataCA_BO_1DTO data) {
        this.date = date;
        this.instanceID = instanceID;
        this.yourInstance = yourInstance;
        this.sender = sender;
        this.yourApp = yourApp;
        this.data = data;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }


    /*public SimpleDateFormat getDate() {
        return date;
    }

    public void setDate(SimpleDateFormat date) {
        this.date = date;
    }*/

    public int getInstanceID() {
        return instanceID;
    }

    public void setInstanceID(int instanceID) {
        this.instanceID = instanceID;
    }

    public int getYourInstance() {
        return yourInstance;
    }

    public void setYourInstance(int yourInstance) {
        this.yourInstance = yourInstance;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getYourApp() {
        return yourApp;
    }

    public void setYourApp(String yourApp) {
        this.yourApp = yourApp;
    }

    public DataCA_BO_1DTO getData() {
        return data;
    }

    public void setData(DataCA_BO_1DTO data) {
        this.data = data;
    }
}
