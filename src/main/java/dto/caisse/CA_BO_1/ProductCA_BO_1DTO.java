package dto.caisse.CA_BO_1;

/**
 * Created by hugo on 20/01/17.
 */
public class ProductCA_BO_1DTO {

    private int quantity = 0;
    private String productRef = "";

    public ProductCA_BO_1DTO() {
    }

    public ProductCA_BO_1DTO(int qte, String ref){
        this.quantity = qte;
        this.productRef = ref;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getProductRef() {
        return productRef;
    }

    public void setProductRef(String productRef) {
        this.productRef = productRef;
    }
}
