package dto.caisse.CA_BO_1;

import model.ProductEntity;

/**
 * Created by claebo_c on 23/01/17.
 */
public class PairCA_BO_1 {
    private ProductEntity productEntity;
    private int quantity;
    private boolean isCommand;

    public PairCA_BO_1() {
    }

    public PairCA_BO_1(ProductEntity productEntity, int quantity, boolean isCommand) {
        this.productEntity = productEntity;
        this.quantity = quantity;
        this.isCommand = isCommand;
    }

    public ProductEntity getProductEntity() {
        return productEntity;
    }

    public void setProductEntity(ProductEntity productEntity) {
        this.productEntity = productEntity;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public boolean isCommand() {
        return isCommand;
    }

    public void setCommand(boolean isCommand) {
        this.isCommand = isCommand;
    }
}
