package dto.caisse.CA_BO_1;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hugo on 20/01/17.
 */
public class DataCA_BO_1DTO {

    private String customerID = "";
    private String receiptID = "";
    private double totalPrice;
    private List<ProductCA_BO_1DTO> products = new ArrayList<ProductCA_BO_1DTO>();

    public DataCA_BO_1DTO() {
    }

    public DataCA_BO_1DTO(String customerID, String receiptID, double totalPrice, List<ProductCA_BO_1DTO> products) {
        this.customerID = customerID;
        this.receiptID = receiptID;
        this.totalPrice = totalPrice;
        this.products = products;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getCustomerID() {
        return customerID;
    }

    public void setCustomerID(String customerID) {
        this.customerID = customerID;
    }

    public String getReceiptID() {
        return receiptID;
    }

    public void setReceiptID(String receiptID) {
        this.receiptID = receiptID;
    }

    public List<ProductCA_BO_1DTO> getProducts() {
        return products;
    }

    public void setProducts(List<ProductCA_BO_1DTO> products) {
        this.products = products;
    }

    public void addProduct (ProductCA_BO_1DTO productCABO1DTO){
        this.products.add(productCABO1DTO);
    }
}
