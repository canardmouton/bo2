package dto.caisse.BO_CA_2;

/**
 * Created by hugo on 20/01/17.
 */
public class StructBO_CA_2DTO {
    private String sender;
    private int instanceID;
    private DataBO_CA_2DTO data = new DataBO_CA_2DTO();

    public StructBO_CA_2DTO() {
    }

    public StructBO_CA_2DTO(String sender, int instanceID, DataBO_CA_2DTO data) {
        this.sender = sender;
        this.instanceID = instanceID;
        this.data = data;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public int getInstanceID() {
        return instanceID;
    }

    public void setInstanceID(int instanceID) {
        this.instanceID = instanceID;
    }

    public DataBO_CA_2DTO getData() {
        return data;
    }

    public void setData(DataBO_CA_2DTO data) {
        this.data = data;
    }
}
