package dto.caisse.BO_CA_2;

import model.ProductEntity;

/**
 * Created by hugo on 20/01/17.
 */
public class ProductBO_CA_2DTO {
    private String productRef;
    private double price;

    public ProductBO_CA_2DTO() {
    }

    public ProductBO_CA_2DTO(String productRef, double price) {
        this.productRef = productRef;
        this.price = price;
    }

    public ProductBO_CA_2DTO(ProductEntity productEntity){
        this.productRef = productEntity.getRefProductRE();
        this.price = productEntity.getPrice();
    }

    public String getProductRef() {
        return productRef;
    }

    public void setProductRef(String productRef) {
        this.productRef = productRef;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
