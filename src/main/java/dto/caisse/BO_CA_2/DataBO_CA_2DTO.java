package dto.caisse.BO_CA_2;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hugo on 20/01/17.
 */
public class DataBO_CA_2DTO {
    private List<ProductBO_CA_2DTO> listProduct = new ArrayList<ProductBO_CA_2DTO>();

    public DataBO_CA_2DTO() {
    }

    public DataBO_CA_2DTO(List<ProductBO_CA_2DTO> listProduct) {
        this.listProduct = listProduct;
    }

    public List<ProductBO_CA_2DTO> getListProduct() {
        return listProduct;
    }

    public void setListProduct(List<ProductBO_CA_2DTO> listProduct) {
        this.listProduct = listProduct;
    }
}
