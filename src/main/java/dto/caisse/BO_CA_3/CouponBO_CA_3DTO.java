package dto.caisse.BO_CA_3;

/**
 * Created by hugo on 23/01/17.
 */
public class CouponBO_CA_3DTO {
    private String couponId;
    private int couponQuantity;
    private String couponDescription;

    public CouponBO_CA_3DTO() {
    }

    public CouponBO_CA_3DTO(String couponId, int couponQuantity, String couponDescription) {
        this.couponId = couponId;
        this.couponQuantity = couponQuantity;
        this.couponDescription = couponDescription;
    }

    public String getCouponId() {
        return couponId;
    }

    public void setCouponId(String couponId) {
        this.couponId = couponId;
    }

    public int getCouponQuantity() {
        return couponQuantity;
    }

    public void setCouponQuantity(int couponQuantity) {
        this.couponQuantity = couponQuantity;
    }

    public String getCouponDescription() {
        return couponDescription;
    }

    public void setCouponDescription(String couponDescription) {
        this.couponDescription = couponDescription;
    }
}
