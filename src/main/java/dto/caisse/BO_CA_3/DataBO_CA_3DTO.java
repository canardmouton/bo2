package dto.caisse.BO_CA_3;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hugo on 23/01/17.
 */
public class DataBO_CA_3DTO {
    private String receiptId;
    private List<CouponBO_CA_3DTO> listCoupon = new ArrayList<CouponBO_CA_3DTO>();

    public DataBO_CA_3DTO() {
    }

    public DataBO_CA_3DTO(String receiptId, List<CouponBO_CA_3DTO> listCoupon) {
        this.receiptId = receiptId;
        this.listCoupon = listCoupon;
    }

    public String getReceiptId() {
        return receiptId;
    }

    public void setReceiptId(String receiptId) {
        this.receiptId = receiptId;
    }

    public List<CouponBO_CA_3DTO> getListCoupon() {
        return listCoupon;
    }

    public void setListCoupon(List<CouponBO_CA_3DTO> listCoupon) {
        this.listCoupon = listCoupon;
    }
}
