package dto.caisse.BO_CA_3;

/**
 * Created by hugo on 23/01/17.
 */
public class StructBO_CA_3DTO {
    private String sender;
    private int instanceId;
    private DataBO_CA_3DTO data = new DataBO_CA_3DTO();

    public StructBO_CA_3DTO() {
    }

    public StructBO_CA_3DTO(String sender, int instanceId, DataBO_CA_3DTO data) {
        this.sender = sender;
        this.instanceId = instanceId;
        this.data = data;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public int getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(int instanceId) {
        this.instanceId = instanceId;
    }

    public DataBO_CA_3DTO getData() {
        return data;
    }

    public void setData(DataBO_CA_3DTO data) {
        this.data = data;
    }
}
