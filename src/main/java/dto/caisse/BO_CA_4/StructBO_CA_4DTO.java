package dto.caisse.BO_CA_4;

/**
 * Created by supercanard on 26/01/17.
 */
public class StructBO_CA_4DTO {
    private int instanceID = 0;
    private String sender;
    private DataBO_CA_4DTO data;

    public StructBO_CA_4DTO(int yourInstance, String sender, DataBO_CA_4DTO dataBO_ca_4DTO) {
        this.instanceID= yourInstance;
        this.sender = sender;
        this.data = dataBO_ca_4DTO;
    }

    public int getYourInstance() {
        return instanceID;
    }

    public void setYourInstance(int yourInstance) {
        this.instanceID = yourInstance;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public DataBO_CA_4DTO getDataBO_ca_4DTO() {
        return data;
    }

    public void setDataBO_ca_4DTO(DataBO_CA_4DTO dataBO_ca_4DTO) {
        this.data = dataBO_ca_4DTO;
    }
}
