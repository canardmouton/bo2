package dto.caisse.BO_CA_4;

import dto.fidelite.FI_BO_2.CouponsFI_BO_2DTO;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by supercanard on 26/01/17.
 */
public class DataBO_CA_4DTO {
    private String refClient;
    private List<CouponsFI_BO_2DTO> listCoupons = new ArrayList<>();

    public DataBO_CA_4DTO(String refClient, List<CouponsFI_BO_2DTO> couponsFI_bo_2DTOS) {
        this.refClient = refClient;
        this.listCoupons = couponsFI_bo_2DTOS;
    }

    public String getRefClient() {
        return refClient;
    }

    public void setRefClient(String refClient) {
        this.refClient = refClient;
    }

    public List<CouponsFI_BO_2DTO> getCouponsFI_bo_2DTOS() {
        return listCoupons;
    }

    public void setCouponsFI_bo_2DTOS(List<CouponsFI_BO_2DTO> couponsFI_bo_2DTOS) {
        this.listCoupons = couponsFI_bo_2DTOS;
    }
}
