package dto.referentiel;

import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hugo on 23/01/17.
 */
public class DataRE_BO_1DTO {
    private List<ProductRE_BO_1DTO> list_product = new ArrayList<ProductRE_BO_1DTO>();


    public DataRE_BO_1DTO() { }

    public DataRE_BO_1DTO(List<ProductRE_BO_1DTO> list_product) {
        this.list_product = list_product;
    }

    public List<ProductRE_BO_1DTO> getList_product() {
        return list_product;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
