package dto.referentiel;

/**
 * Created by hugo on 23/01/17.
 */
public class StructRE_BO_1DTO {
    private String date;

    private String yourApp = "";
    private int yourInstance = 0;
    private String sender;
    private int instanceID;

    private DataRE_BO_1DTO data = new DataRE_BO_1DTO();


    public StructRE_BO_1DTO() {
    }

    public StructRE_BO_1DTO(String date, int instanceID, int yourInstance, String sender, String yourApp, DataRE_BO_1DTO data) {
        this.date = date;
        this.instanceID = instanceID;
        this.yourInstance = yourInstance;
        this.sender = sender;
        this.yourApp = yourApp;
        this.data = data;
    }

    public DataRE_BO_1DTO getData() {
        return data;
    }

    public void setData(DataRE_BO_1DTO data) {
        this.data = data;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public int getInstanceID() {
        return instanceID;
    }

    public void setInstanceID(int instanceID) {
        this.instanceID = instanceID;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getYourApp() {
        return yourApp;
    }

    public void setYourApp(String yourApp) {
        this.yourApp = yourApp;
    }

    public int getYourInstance() {
        return yourInstance;
    }

    public void setYourInstance(int yourInstance) {
        this.yourInstance = yourInstance;
    }
}
