package dto.referentiel;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Created by hugo on 23/01/17.
 */
public class ProductRE_BO_1DTO {
    //private String id_product; // OK
    private String id_product_RE;
    private String id_product_FO;
    private int ref_provider; // OK
    private String provider_name; // OK
    private double sale_price; // OK
    //private int promo;
    //private String begin_date_promo;
    //private String end_date_promo;
    private String description; // OK
    private String category; // OK
    private boolean isAssortment;

    public ProductRE_BO_1DTO() {}

    public ProductRE_BO_1DTO(String id_product_RE, String id_product_FO, int ref_provider, String provider_name, double sale_price, String description, String category, boolean is_assortment) {
        this.id_product_RE = id_product_RE;
        this.id_product_FO = id_product_FO;
        this.ref_provider = ref_provider;
        this.provider_name = provider_name;
        this.sale_price = sale_price;
        this.description = description;
        this.category = category;
        this.isAssortment = is_assortment;
    }

    public String getId_product_RE() {
        return id_product_RE;
    }

    public void setId_product_RE(String id_product_RE) {
        this.id_product_RE = id_product_RE;
    }

    public String getId_product_FO() {
        return id_product_FO;
    }

    public void setId_product_FO(String id_product_FO) {
        this.id_product_FO = id_product_FO;
    }

    public int getRef_provider() {
        return ref_provider;
    }

    public void setRef_provider(int ref_provider) {
        this.ref_provider = ref_provider;
    }

    public String getProvider_name() {
        return provider_name;
    }

    public void setProvider_name(String provider_name) {
        this.provider_name = provider_name;
    }

    public double getSale_price() {
        return sale_price;
    }

    public void setSale_price(double sale_price) {
        this.sale_price = sale_price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public boolean isIs_assortment() {
        return isAssortment;
    }

    public void setIs_assortment(boolean is_assortment) {
        this.isAssortment = is_assortment;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
