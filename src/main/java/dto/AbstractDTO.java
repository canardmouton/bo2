package dto;

/**
 * Created by claebo_c on 19/01/17.
 */
public class AbstractDTO<E> {
    private String sender = "";
    private int instanceID = 0;
    private E data = null;

    public AbstractDTO() {
    }

    public AbstractDTO(String sender, int instanceID, E data) {
        this.sender = sender;
        this.instanceID = instanceID;
        this.data = data;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public int getInstanceID() {
        return instanceID;
    }

    public void setInstanceID(int instanceID) {
        this.instanceID = instanceID;
    }

    public E getData() {
        return data;
    }

    public void setData(E data) {
        this.data = data;
    }
}
