package dto;

import java.util.List;

/**
 * Created by supercanard on 14/01/17.
 */
public class ListProductBoUtDTO {

    private List<ProductBoUtDTO> productBoUtDTOS;

    public ListProductBoUtDTO() {
    }

    public List<ProductBoUtDTO> getProductBoUtDTOS() {
        return productBoUtDTOS;
    }

    public void setProductBoUtDTOS(List<ProductBoUtDTO> productBoUtDTOS) {
        this.productBoUtDTOS = productBoUtDTOS;
    }
}
