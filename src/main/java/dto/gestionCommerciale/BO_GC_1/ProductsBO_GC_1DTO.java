package dto.gestionCommerciale.BO_GC_1;

/**
 * Created by hugo on 20/01/17.
 */
public class ProductsBO_GC_1DTO {
    private String ref_product;
    private int quantity;

    public ProductsBO_GC_1DTO() {
    }

    public ProductsBO_GC_1DTO(String ref_product, int quantity) {
        this.ref_product = ref_product;
        this.quantity = quantity;
    }

    public String getRef_product() {
        return ref_product;
    }

    public void setRef_product(String ref_product) {
        this.ref_product = ref_product;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
