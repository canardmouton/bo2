package dto.gestionCommerciale.BO_GC_1;

/**
 * Created by hugo on 20/01/17.
 */
public class StructBO_GC_1DTO {
    private String sender;
    private int instanceID;
    private DataBO_GC_1DTO data = new DataBO_GC_1DTO();

    public StructBO_GC_1DTO() {
    }

    public StructBO_GC_1DTO(String sender, int instanceID, DataBO_GC_1DTO data) {
        this.sender = sender;
        this.instanceID = instanceID;
        this.data = data;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public int getInstanceID() {
        return instanceID;
    }

    public void setInstanceID(int instanceID) {
        this.instanceID = instanceID;
    }

    public DataBO_GC_1DTO getData() {
        return data;
    }

    public void setData(DataBO_GC_1DTO data) {
        this.data = data;
    }
}
