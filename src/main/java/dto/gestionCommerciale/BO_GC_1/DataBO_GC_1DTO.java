package dto.gestionCommerciale.BO_GC_1;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hugo on 20/01/17.
 */
public class DataBO_GC_1DTO {
    private String ref_delivery;
    private List<ProductsBO_GC_1DTO> liste_products = new ArrayList<ProductsBO_GC_1DTO>();

    public DataBO_GC_1DTO() {
    }

    public DataBO_GC_1DTO(String ref_delivery, List<ProductsBO_GC_1DTO> liste_products) {
        this.ref_delivery = ref_delivery;
        this.liste_products = liste_products;
    }

    public void addProduct (ProductsBO_GC_1DTO product) {
        liste_products.add(product);

    }
    public String getRef_delivery() {
        return ref_delivery;
    }

    public void setRef_delivery(String ref_delivery) {
        this.ref_delivery = ref_delivery;
    }

    public List<ProductsBO_GC_1DTO> getListe_products() {
        return liste_products;
    }

    public void setListe_products(List<ProductsBO_GC_1DTO> liste_products) {
        this.liste_products = liste_products;
    }
}
