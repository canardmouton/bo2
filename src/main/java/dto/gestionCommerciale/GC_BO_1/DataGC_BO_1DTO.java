package dto.gestionCommerciale.GC_BO_1;

import org.apache.commons.lang.builder.ToStringBuilder;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by hugo on 20/01/17.
 */
public class DataGC_BO_1DTO {
    private String date;
    private String ref_reassort;
    private List<ProductGC_BO_1DTO> liste_product = new ArrayList<ProductGC_BO_1DTO>();

    public DataGC_BO_1DTO() {
    }

    public DataGC_BO_1DTO(String date, String ref_command, List<ProductGC_BO_1DTO> liste_product) {
        this.date = date;
        this.ref_reassort = ref_command;
        this.liste_product = liste_product;
    }


    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getRef_command() {
        return ref_reassort;
    }

    public void setRef_command(String ref_command) {
        this.ref_reassort = ref_command;
    }

    public List<ProductGC_BO_1DTO> getListe_product() {
        return liste_product;
    }

    public void setListe_product(List<ProductGC_BO_1DTO> liste_product) {
        this.liste_product = liste_product;
    }
}
