package dto.gestionCommerciale.GC_BO_1;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Created by hugo on 20/01/17.
 */
public class StructGC_BO_1DTO {
    private String date;
    private int instanceID;
    private int yourInstance = 0;
    private String sender;
    private String yourApp = "";
    private DataGC_BO_1DTO data = new DataGC_BO_1DTO();

    public StructGC_BO_1DTO() {
    }

    public StructGC_BO_1DTO(String date, String yourApp, int yourInstance, String sender, int instanceID, DataGC_BO_1DTO data) {
        this.date = date;
        this.yourApp = yourApp;
        this.yourInstance = yourInstance;
        this.sender = sender;
        this.instanceID = instanceID;
        this.data = data;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getYourApp() {
        return yourApp;
    }

    public void setYourApp(String yourApp) {
        this.yourApp = yourApp;
    }

    public int getYourInstance() {
        return yourInstance;
    }

    public void setYourInstance(int yourInstance) {
        this.yourInstance = yourInstance;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public int getInstanceID() {
        return instanceID;
    }

    public void setInstanceID(int instanceID) {
        this.instanceID = instanceID;
    }

    public DataGC_BO_1DTO getData() {
        return data;
    }

    public void setData(DataGC_BO_1DTO data) {
        this.data = data;
    }
}
