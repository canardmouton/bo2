package dto.gestionCommerciale;

/**
 * Created by hugo on 20/01/17.
 */
public class CommandDTO {

    private String sender;
    private int instanceID;
    private CommandDataDTO data = new CommandDataDTO();

    public CommandDTO() {
    }

    public CommandDTO(String sender, int instanceID, CommandDataDTO data) {
        this.sender = sender;
        this.instanceID = instanceID;
        this.data = data;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public int getInstanceID() {
        return instanceID;
    }

    public void setInstanceID(int instanceID) {
        this.instanceID = instanceID;
    }

    public CommandDataDTO getData() {
        return data;
    }

    public void setData(CommandDataDTO data) {
        this.data = data;
    }
}
