package dto.gestionCommerciale.BO_GC_3;

import model.CommandDetailEntity;

/**
 * Created by hugo on 20/01/17.
 */

public class ProductsBO_GC_3DTO {
    private String refProduct;
   private int quantity;

    public ProductsBO_GC_3DTO() {
    }

    public ProductsBO_GC_3DTO(CommandDetailEntity commandDetailEntity) {
        this.refProduct = commandDetailEntity.getProduct().getRefProductRE();
        this.quantity = commandDetailEntity.getQuantite();
    }

    public ProductsBO_GC_3DTO(String refProduct, int quantity) {
        this.refProduct = refProduct;
        this.quantity = quantity;
    }

    public String getRefProduct() {
        return refProduct;
    }

    public void setRefProduct(String refProduct) {
        this.refProduct = refProduct;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
