package dto.gestionCommerciale.BO_GC_3;

import model.CommandDetailEntity;
import model.CommandEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by hugo on 20/01/17.
 */

public class DataBO_GC_3DTO {
    private String refClient;
    private String date;
    private List<ProductsBO_GC_3DTO> listProducts = new ArrayList<ProductsBO_GC_3DTO>();

    public DataBO_GC_3DTO() {
    }

    public DataBO_GC_3DTO(CommandEntity commandEntity, Set<CommandDetailEntity> commandDetailEntities) {
        this.refClient = commandEntity.getClient().getRefClient();
        this.date = commandEntity.getDateEnvoi().toString();
        for (CommandDetailEntity entity : commandDetailEntities){
            listProducts.add(new ProductsBO_GC_3DTO(entity));
        }
    }

    public DataBO_GC_3DTO(String refClient, String date, List<ProductsBO_GC_3DTO> listProducts) {
        this.refClient = refClient;
        this.date = date;
        this.listProducts = listProducts;
    }

    public String getRefClient() {
        return refClient;
    }

    public void setRefClient(String refClient) {
        this.refClient = refClient;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<ProductsBO_GC_3DTO> getListProducts() {
        return listProducts;
    }

    public void setListProducts(List<ProductsBO_GC_3DTO> listProducts) {
        this.listProducts = listProducts;
   }
}
