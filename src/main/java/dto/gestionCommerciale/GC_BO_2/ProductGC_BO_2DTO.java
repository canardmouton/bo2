package dto.gestionCommerciale.GC_BO_2;

/**
 * Created by hugo on 20/01/17.
 */
public class ProductGC_BO_2DTO {
    private String ref_product;
    private int quantity;

    public ProductGC_BO_2DTO() {
    }

    public ProductGC_BO_2DTO(String ref_product, int quantity) {
        this.ref_product = ref_product;
        this.quantity = quantity;
    }

    public String getRef_product() {
        return ref_product;
    }

    public void setRef_product(String ref_product) {
        this.ref_product = ref_product;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
