package dto.gestionCommerciale.GC_BO_2;

/**
 * Created by hugo on 20/01/17.
 */
public class StructGC_BO_2DTO {

    private String date;
    private int instanceID;
    private int yourInstance = 0;
    private String sender;
    private String yourApp = "";
    private DataGC_BO_2DTO data = new DataGC_BO_2DTO();

    public StructGC_BO_2DTO() {
    }

    public StructGC_BO_2DTO(String date, int instanceID, int yourInstance, String sender, String yourApp, DataGC_BO_2DTO data) {
        this.date = date;
        this.instanceID = instanceID;
        this.yourInstance = yourInstance;
        this.sender = sender;
        this.yourApp = yourApp;
        this.data = data;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getInstanceID() {
        return instanceID;
    }

    public void setInstanceID(int instanceID) {
        this.instanceID = instanceID;
    }

    public int getYourInstance() {
        return yourInstance;
    }

    public void setYourInstance(int yourInstance) {
        this.yourInstance = yourInstance;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getYourApp() {
        return yourApp;
    }

    public void setYourApp(String yourApp) {
        this.yourApp = yourApp;
    }

    public DataGC_BO_2DTO getData() {
        return data;
    }

    public void setData(DataGC_BO_2DTO data) {
        this.data = data;
    }
}