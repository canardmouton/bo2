package dto.gestionCommerciale.GC_BO_2;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hugo on 20/01/17.
 */
public class DataGC_BO_2DTO {
    private String date;
    private String ref_command;
    private List<ProductGC_BO_2DTO> liste_products = new ArrayList<ProductGC_BO_2DTO>();

    public DataGC_BO_2DTO() {
    }

    public DataGC_BO_2DTO(String date, int type_command, String ref_command, List<ProductGC_BO_2DTO> liste_products) {
        this.date = date;
        this.ref_command = ref_command;
        this.liste_products = liste_products;
    }


    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }


    public String getRef_command() {
        return ref_command;
    }

    public void setRef_command(String ref_command) {
        this.ref_command = ref_command;
    }

    public List<ProductGC_BO_2DTO> getListe_products() {
        return liste_products;
    }

    public void setListe_products(List<ProductGC_BO_2DTO> liste_products) {
        this.liste_products = liste_products;
    }
}
