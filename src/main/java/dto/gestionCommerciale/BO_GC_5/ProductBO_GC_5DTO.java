package dto.gestionCommerciale.BO_GC_5;

import dto.caisse.CA_BO_1.PairCA_BO_1;

/**
 * Created by hugo on 20/01/17.
 */
public class ProductBO_GC_5DTO {
    private String ref_product;
    private int quantity;
    private boolean isCommand;

    public ProductBO_GC_5DTO() {
    }

    public ProductBO_GC_5DTO(String ref_product, int quantity, boolean isCommand) {
        this.ref_product = ref_product;
        this.quantity = quantity;
        this.isCommand = isCommand;
    }

    public ProductBO_GC_5DTO(PairCA_BO_1 pairCA_bo_1){
        this.ref_product = pairCA_bo_1.getProductEntity().getRefProductRE();
        this.quantity = pairCA_bo_1.getQuantity();
        this.isCommand = pairCA_bo_1.isCommand();
    }

    public String getRef_product() {
        return ref_product;
    }

    public void setRef_product(String ref_product) {
        this.ref_product = ref_product;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public boolean isCommand() {
        return isCommand;
    }

    public void setCommand(boolean command) {
        isCommand = command;
    }
}
