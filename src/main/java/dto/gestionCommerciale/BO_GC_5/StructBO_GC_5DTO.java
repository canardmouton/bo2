package dto.gestionCommerciale.BO_GC_5;

/**
 * Created by hugo on 20/01/17.
 */
public class StructBO_GC_5DTO {
    private String sender;
    private int instanceID;
    private ListDataBO_GC_5DTO data = new ListDataBO_GC_5DTO();

    public StructBO_GC_5DTO() {
    }

    public StructBO_GC_5DTO(String sender, int instanceID, ListDataBO_GC_5DTO data) {
        this.sender = sender;
        this.instanceID = instanceID;
        this.data = data;
    }

    public void addData(DataBO_GC_5DTO dataBO_gc_5DTO){
        this.data.addData(dataBO_gc_5DTO);
    }
    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public int getInstanceID() {
        return instanceID;
    }

    public void setInstanceID(int instanceID) {
        this.instanceID = instanceID;
    }

    public ListDataBO_GC_5DTO getData() {
        return data;
    }

    public void setData(ListDataBO_GC_5DTO data) {
        this.data = data;
    }
}
