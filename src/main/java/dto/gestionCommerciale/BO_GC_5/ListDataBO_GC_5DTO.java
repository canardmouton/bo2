package dto.gestionCommerciale.BO_GC_5;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hugo on 24/01/17.
 */
public class ListDataBO_GC_5DTO {
    private List<DataBO_GC_5DTO> liste_data = new ArrayList<>();

    public ListDataBO_GC_5DTO(List<DataBO_GC_5DTO> liste_data) {
        this.liste_data = liste_data;
    }

    public ListDataBO_GC_5DTO() { }

    public void addData (DataBO_GC_5DTO dataBO_gc_5DTO){
        this.liste_data.add(dataBO_gc_5DTO);
    }
}
