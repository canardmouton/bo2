package dto.gestionCommerciale.BO_GC_5;

import dto.caisse.CA_BO_1.PairCA_BO_1;
import model.ClientEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hugo on 20/01/17.
 */
public class DataBO_GC_5DTO {
    private String adresse;
    private List<ProductBO_GC_5DTO> liste_product = new ArrayList<ProductBO_GC_5DTO>();

    public DataBO_GC_5DTO() {
    }

    public DataBO_GC_5DTO(String adresse, List<ProductBO_GC_5DTO> liste_product) {
        this.adresse = adresse;
        this.liste_product = liste_product;
    }

    public DataBO_GC_5DTO(List<PairCA_BO_1> list, ClientEntity clientEntity){
        this.adresse = clientEntity.getRefClient();
        for (PairCA_BO_1 pair : list){
            this.liste_product.add(new ProductBO_GC_5DTO(pair));
        }
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public List<ProductBO_GC_5DTO> getListe_product() {
        return liste_product;
    }

    public void setListe_product(List<ProductBO_GC_5DTO> liste_product) {
        this.liste_product = liste_product;
    }
}
