package dto.gestionCommerciale;

/**
 * Created by hugo on 20/01/17.
 */
public class CommandDataDTO {
    private CommandCommandDTO command = new CommandCommandDTO();

    public CommandDataDTO() {
    }

    public CommandDataDTO(CommandCommandDTO command) {
        this.command = command;
    }

    public CommandCommandDTO getCommand() {
        return command;
    }

    public void setCommand(CommandCommandDTO command) {
        this.command = command;
    }
}
