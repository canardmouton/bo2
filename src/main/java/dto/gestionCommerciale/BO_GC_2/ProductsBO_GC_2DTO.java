package dto.gestionCommerciale.BO_GC_2;

import dto.gestionCommerciale.GC_BO_1.ProductGC_BO_1DTO;

/**
 * Created by hugo on 20/01/17.
 */
public class ProductsBO_GC_2DTO {
    private String ref_product;
    private int quantity;

    public ProductsBO_GC_2DTO() {
    }

    public ProductsBO_GC_2DTO(ProductGC_BO_1DTO productGCBo1DTO){
        this.ref_product = productGCBo1DTO.getRef_product();
        this.quantity = productGCBo1DTO.getQuantity();
    }

    public ProductsBO_GC_2DTO(String ref_product, int quantity) {
        this.ref_product = ref_product;
        this.quantity = quantity;
    }

    public String getRef_product() {
        return ref_product;
    }

    public void setRef_product(String ref_product) {
        this.ref_product = ref_product;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
