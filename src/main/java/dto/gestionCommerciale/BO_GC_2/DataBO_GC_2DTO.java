package dto.gestionCommerciale.BO_GC_2;

import dto.gestionCommerciale.GC_BO_1.DataGC_BO_1DTO;
import dto.gestionCommerciale.GC_BO_1.ProductGC_BO_1DTO;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hugo on 20/01/17.
 */
public class DataBO_GC_2DTO {
    private String ref_command;
    private String date;
    private List<ProductsBO_GC_2DTO> liste_products = new ArrayList<ProductsBO_GC_2DTO>();

    public DataBO_GC_2DTO() {
    }

    public DataBO_GC_2DTO(DataGC_BO_1DTO dataGC_bo_1DTO){
        this.date = dataGC_bo_1DTO.getDate();
        this.ref_command = dataGC_bo_1DTO.getRef_command();
        for (ProductGC_BO_1DTO productGCBo1DTO : dataGC_bo_1DTO.getListe_product()){
            this.liste_products.add(new ProductsBO_GC_2DTO(productGCBo1DTO));
        }
    }

    public DataBO_GC_2DTO(String ref_reassort, String date, List<ProductsBO_GC_2DTO> liste_products) {
        this.ref_command = ref_reassort;
        this.date = date;
        this.liste_products = liste_products;
    }

    public String getRef_reassort() {
        return ref_command;
    }

    public void setRef_reassort(String ref_reassort) {
        this.ref_command = ref_reassort;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<ProductsBO_GC_2DTO> getListe_products() {
        return liste_products;
    }

    public void setListe_products(List<ProductsBO_GC_2DTO> liste_products) {
        this.liste_products = liste_products;
    }
}
