package dto.gestionCommerciale;

/**
 * Created by hugo on 20/01/17.
 */
public class CommandProductDTO {

    private String codeProduit = "";
    private int quantite;

    public CommandProductDTO() {
    }

    public CommandProductDTO(String codeProduit, int quantite) {
        this.codeProduit = codeProduit;
        this.quantite = quantite;
    }

    public String getCodeProduit() {
        return codeProduit;
    }

    public void setCodeProduit(String codeProduit) {
        this.codeProduit = codeProduit;
    }

    public int getQuantite() {
        return quantite;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }
}
