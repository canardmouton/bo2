package dto.gestionCommerciale;

import java.text.SimpleDateFormat;

/**
 * Created by hugo on 20/01/17.
 */
public class CommandCommandDTO {

    private String numeroCommand;
    private SimpleDateFormat dateLivraison = new SimpleDateFormat("yyyy-MM-dd");
    private CommandProductDTO items = new CommandProductDTO();

    public CommandCommandDTO() {
    }

    public CommandCommandDTO(String numeroCommand, SimpleDateFormat dateLivraison, CommandProductDTO items) {
        this.numeroCommand = numeroCommand;
        this.dateLivraison = dateLivraison;
        this.items = items;
    }

    public String getNumeroCommand() {
        return numeroCommand;
    }

    public void setNumeroCommand(String numeroCommand) {
        this.numeroCommand = numeroCommand;
    }

    public SimpleDateFormat getDateLivraison() {
        return dateLivraison;
    }

    public void setDateLivraison(SimpleDateFormat dateLivraison) {
        this.dateLivraison = dateLivraison;
    }

    public CommandProductDTO getItems() {
        return items;
    }

    public void setItems(CommandProductDTO items) {
        this.items = items;
    }
}
