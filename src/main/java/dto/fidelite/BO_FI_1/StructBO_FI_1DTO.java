package dto.fidelite.BO_FI_1;

/**
 * Created by hugo on 20/01/17.
 */
public class StructBO_FI_1DTO {
    private String sender;
    private int instanceID;
    private DataBO_FI_1DTO data = new DataBO_FI_1DTO();

    public StructBO_FI_1DTO() {
    }

    public StructBO_FI_1DTO(String sender, int instanceID, DataBO_FI_1DTO data) {
        this.sender = sender;
        this.instanceID = instanceID;
        this.data = data;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public int getInstanceID() {
        return instanceID;
    }

    public void setInstanceID(int instanceID) {
        this.instanceID = instanceID;
    }

    public DataBO_FI_1DTO getData() {
        return data;
    }

    public void setData(DataBO_FI_1DTO data) {
        this.data = data;
    }
}
