package dto.fidelite.BO_FI_1;

import dto.caisse.CA_BO_1.DataCA_BO_1DTO;
import dto.caisse.CA_BO_1.ProductCA_BO_1DTO;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hugo on 20/01/17.
 */
public class DataBO_FI_1DTO {
    private String refCaisse;
    private String refClient;
    private String receiptId;
    private List<ProductBO_FI_1DTO> listProduct = new ArrayList<ProductBO_FI_1DTO>();

    public DataBO_FI_1DTO() {
    }

    public DataBO_FI_1DTO(String refCaisse, String refClient, String receiptId, List<ProductBO_FI_1DTO> listProduct) {
        this.refCaisse = refCaisse;
        this.refClient = refClient;
        this.receiptId = receiptId;
        this.listProduct = listProduct;
    }

    public DataBO_FI_1DTO(String refCaisse, DataCA_BO_1DTO dataCA_bo_1DTO){
        this.refCaisse = refCaisse;
        this.receiptId = dataCA_bo_1DTO.getReceiptID();
        this.refClient = dataCA_bo_1DTO.getCustomerID();
        for (ProductCA_BO_1DTO product : dataCA_bo_1DTO.getProducts()){
            listProduct.add(new ProductBO_FI_1DTO(product));
        }

    }

    public String getRefCaisse() {
        return refCaisse;
    }

    public void setRefCaisse(String refCaisse) {
        this.refCaisse = refCaisse;
    }

    public String getRefClient() {
        return refClient;
    }

    public void setRefClient(String refClient) {
        this.refClient = refClient;
    }

    public String getReceiptId() {
        return receiptId;
    }

    public void setReceiptId(String receiptId) {
        this.receiptId = receiptId;
    }

    public List<ProductBO_FI_1DTO> getListProduct() {
        return listProduct;
    }

    public void setListProduct(List<ProductBO_FI_1DTO> listProduct) {
        this.listProduct = listProduct;
    }
}
