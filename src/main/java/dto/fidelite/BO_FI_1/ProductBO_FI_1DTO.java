package dto.fidelite.BO_FI_1;

import dto.caisse.CA_BO_1.ProductCA_BO_1DTO;

/**
 * Created by hugo on 20/01/17.
 */
public class ProductBO_FI_1DTO {
    private String refProduct;
    private int quantity;
    private boolean isCommand;

    public ProductBO_FI_1DTO() {
    }

    public ProductBO_FI_1DTO(String ref_product, int quantity, boolean isCommand) {
        this.refProduct = ref_product;
        this.quantity = quantity;
        this.isCommand = isCommand;
    }

    public ProductBO_FI_1DTO(ProductCA_BO_1DTO productCA_bo_1DTO){
        this.refProduct = productCA_bo_1DTO.getProductRef();
        this.quantity = productCA_bo_1DTO.getQuantity();
    }

    public String getRefProduct() {
        return refProduct;
    }

    public void setRefProduct(String refProduct) {
        this.refProduct = refProduct;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public boolean isCommand() {
        return isCommand;
    }

    public void setCommand(boolean command) {
        isCommand = command;
    }
}

