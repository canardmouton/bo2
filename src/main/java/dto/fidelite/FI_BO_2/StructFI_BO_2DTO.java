package dto.fidelite.FI_BO_2;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hugo on 21/01/17.
 */
public class StructFI_BO_2DTO {
    private String date;
    private int instanceID;
    private int yourInstance = 0;
    private String sender;
    private String yourApp = "";
    private DataFI_BO_2DTO data = new DataFI_BO_2DTO();

    public StructFI_BO_2DTO() {
    }

    public StructFI_BO_2DTO(String date, String yourApp, int yourInstance, String sender, int instanceID, DataFI_BO_2DTO data) {
        this.date = date;
        this.yourApp = yourApp;
        this.yourInstance = yourInstance;
        this.sender = sender;
        this.instanceID = instanceID;
        this.data = data;
    }


    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getYourApp() {
        return yourApp;
    }

    public void setYourApp(String yourApp) {
        this.yourApp = yourApp;
    }

    public int getYourInstance() {
        return yourInstance;
    }

    public void setYourInstance(int yourInstance) {
        this.yourInstance = yourInstance;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public int getInstanceID() {
        return instanceID;
    }

    public void setInstanceID(int instanceID) {
        this.instanceID = instanceID;
    }

    public DataFI_BO_2DTO getData() {
        return data;
    }

    public void setData(DataFI_BO_2DTO data) {
        this.data = data;
    }
}


