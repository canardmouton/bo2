package dto.fidelite.FI_BO_2;

/**
 * Created by hugo on 21/01/17.
 */
public class CouponsFI_BO_2DTO {
    private String type_produit;
    private int discount;

    public CouponsFI_BO_2DTO() {
    }

    public CouponsFI_BO_2DTO(String type_produit, int discount) {
        this.type_produit = type_produit;
        this.discount = discount;
    }




    public String getType_produit() {
        return type_produit;
    }

    public void setType_produit(String type_produit) {
        this.type_produit = type_produit;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }
}
