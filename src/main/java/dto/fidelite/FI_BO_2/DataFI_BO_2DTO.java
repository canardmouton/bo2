package dto.fidelite.FI_BO_2;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hugo on 23/01/17.
 */
public class DataFI_BO_2DTO {
    private String refClient;
    private String refCaisse;
    private List<CouponsFI_BO_2DTO> listCoupons = new ArrayList<CouponsFI_BO_2DTO>();


    public DataFI_BO_2DTO() {
    }

    public DataFI_BO_2DTO(String refClient, String refCaisse, List<CouponsFI_BO_2DTO> liste_coupons) {
        this.refClient = refClient;
        this.refCaisse = refCaisse;
        this.listCoupons = liste_coupons;
    }

    public String getRefClient() {
        return refClient;
    }

    public void setRefClient(String refClient) {
        this.refClient = refClient;
    }

    public String getRefCaisse() {
        return refCaisse;
    }

    public void setRefCaisse(String refCaisse) {
        this.refCaisse = refCaisse;
    }

    public List<CouponsFI_BO_2DTO> getListe_coupons() {
        return listCoupons;
    }

    public void setListe_coupons(List<CouponsFI_BO_2DTO> liste_coupons) {
        this.listCoupons = liste_coupons;
    }
}
