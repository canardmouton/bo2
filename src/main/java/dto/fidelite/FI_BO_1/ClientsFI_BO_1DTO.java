package dto.fidelite.FI_BO_1;

/**
 * Created by hugo on 21/01/17.
 */
public class ClientsFI_BO_1DTO {
    private String ref_client;
    private String nom;
    private String prenom;
    private int credit;
    private int loyalty_point;
    private boolean payment_authorised;

    public ClientsFI_BO_1DTO() {
    }

    public ClientsFI_BO_1DTO(String ref_client, String nom, String prenom, int credit, int loyalty_point, boolean payment_authorised) {
        this.ref_client = ref_client;
        this.nom = nom;
        this.prenom = prenom;
        this.credit = credit;
        this.loyalty_point = loyalty_point;
        this.payment_authorised = payment_authorised;
    }

    public String getRef_client() {
        return ref_client;
    }

    public void setRef_client(String ref_client) {
        this.ref_client = ref_client;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public int getCredit() {
        return credit;
    }

    public void setCredit(int credit) {
        this.credit = credit;
    }

    public int getLoyalty_point() {
        return loyalty_point;
    }

    public void setLoyalty_point(int loyalty_point) {
        this.loyalty_point = loyalty_point;
    }

    public boolean isPayment_authorised() {
        return payment_authorised;
    }

    public void setPayment_authorised(boolean payment_authorised) {
        this.payment_authorised = payment_authorised;
    }
}
