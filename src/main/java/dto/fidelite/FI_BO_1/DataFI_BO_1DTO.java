package dto.fidelite.FI_BO_1;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hugo on 21/01/17.
 */
public class DataFI_BO_1DTO {
    private List<ClientsFI_BO_1DTO> liste_clients = new ArrayList<ClientsFI_BO_1DTO>();

    public DataFI_BO_1DTO() {
    }

    public DataFI_BO_1DTO(List<ClientsFI_BO_1DTO> liste_clients) {
        this.liste_clients = liste_clients;
    }

    public List<ClientsFI_BO_1DTO> getListe_clients() {
        return liste_clients;
    }

    public void setListe_clients(List<ClientsFI_BO_1DTO> liste_clients) {
        this.liste_clients = liste_clients;
    }
}
