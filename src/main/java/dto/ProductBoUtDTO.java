package dto;

import model.ProductEntity;

/**
 * Created by supercanard on 14/01/17.
 */
public class ProductBoUtDTO {
//BO-UT-1
    private int productId;
    private String description;

    private String ref_product_fournisseur;
    private int fournisseur;
    private int quantity;

    public ProductBoUtDTO() {
    }

    public ProductBoUtDTO(ProductEntity productEntity) {
        this.productId = productEntity.getProductId();
        this.description = productEntity.getDescription();
//        this.ref_product_fournisseur = productEntity.getRef_product_fournisseur();
//        this.fournisseur = productEntity.getFournisseur();
        this.quantity = productEntity.getQuantite();
    }
    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRef_product_fournisseur() {
        return ref_product_fournisseur;
    }

    public void setRef_product_fournisseur(String ref_product_fournisseur) {
        this.ref_product_fournisseur = ref_product_fournisseur;
    }

    public int getFournisseur() {
        return fournisseur;
    }

    public void setFournisseur(int fournisseur) {
        this.fournisseur = fournisseur;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
