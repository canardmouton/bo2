DROP DATABASE IF EXISTS "bo2";

CREATE DATABASE "bo2"
WITH ENCODING='UTF8'
OWNER ursi;



DROP TABLE IF EXISTS "client" CASCADE ;
DROP TABLE IF EXISTS "command" CASCADE ;
DROP TABLE IF EXISTS "command_detail" CASCADE ;
DROP TABLE IF EXISTS "local_promotion" CASCADE ;
DROP TABLE IF EXISTS "product" CASCADE ;
DROP TABLE IF EXISTS "product_promotion" CASCADE ;
DROP TABLE IF EXISTS "rule" CASCADE ;
DROP TABLE IF EXISTS "simple_promotion" CASCADE ;

-- Table: "client"

CREATE TABLE "client"
(
  client_id serial NOT NULL,
  ref_client character varying(64) NOT NULL UNIQUE,
  CONSTRAINT client_id PRIMARY KEY (client_id)
);

-- Table: "command"

CREATE TABLE "command"
(
  command_id SERIAL NOT NULL,
  ref_command character varying(32) NOT NULL UNIQUE,
  date_envoi date NOT NULL,
  date_reception date,
  ref_client_id integer,
  CONSTRAINT command_id PRIMARY KEY (command_id),
  CONSTRAINT ref_client_id FOREIGN KEY (ref_client_id)
    REFERENCES "client" (client_id) MATCH SIMPLE
    ON UPDATE  CASCADE ON DELETE CASCADE
);

-- Table: "simple_promotion"

CREATE TABLE "simple_promotion"
(
  simple_promotion_id serial NOT NULL,
  date_start date NOT NULL,
  date_stop date NOT NULL,
  unite FLOAT NOT NULL,
  CONSTRAINT simple_promotion_id PRIMARY KEY (simple_promotion_id)
);

-- Table: "product"

CREATE TABLE "product"
(
  product_id SERIAL PRIMARY KEY,
  ref_product_re character varying(32) NOT NULL UNIQUE,
  ref_product_fo character varying(32) NOT NULL UNIQUE,
  description character varying(64) NOT NULL,
  quantite integer NOT NULL,
  price double precision NOT NULL,
  stock_min integer NOT NULL,
  stock_max integer NOT NULL,
  ref_product_fournisseur INT NOT NULL,
  ref_simple_promotion_id INT,
  fournisseur VARCHAR(32) NOT NULL,
  CONSTRAINT ref_simple_promotion_id FOREIGN KEY (ref_simple_promotion_id)
  REFERENCES "simple_promotion" (simple_promotion_id) MATCH SIMPLE
  ON UPDATE CASCADE ON DELETE CASCADE
);

-- Table: "command_detail"

CREATE TABLE "command_detail"
(
  command_detail_id SERIAL NOT NULL,
  ref_command_id integer NOT NULL,
  ref_product_id integer NOT NULL,
  quantite integer NOT NULL,
  pertes integer NOT NULL,
  CONSTRAINT command_detail_id PRIMARY KEY (command_detail_id),
  CONSTRAINT ref_command_id FOREIGN KEY (ref_command_id)
    REFERENCES "command" (command_id) MATCH SIMPLE
    ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT ref_product_id FOREIGN KEY (ref_product_id)
  REFERENCES "product" (product_id) MATCH SIMPLE
  ON UPDATE CASCADE ON DELETE CASCADE
);

-- Table: "rule"

CREATE TABLE "rule"
(
  rule_id SERIAL NOT NULL,
  montant double precision NOT NULL,
  resultat double precision,
  unit TEXT NOT NULL,
  CONSTRAINT rule_id PRIMARY KEY (rule_id)
);

-- Table: "local_promotion"

CREATE TABLE "local_promotion"
(
  local_promotion_id SERIAL NOT NULL,
  ref_local_promotion character varying(32) NOT NULL UNIQUE,
  designation character(64) NOT NULL,
  ref_id_rule integer NOT NULL,
  recurence boolean NOT NULL,
  CONSTRAINT local_promotion_id PRIMARY KEY (local_promotion_id),
  CONSTRAINT ref_id_rule FOREIGN KEY (ref_id_rule)
    REFERENCES "rule" (rule_id) MATCH SIMPLE
    ON UPDATE CASCADE ON DELETE CASCADE
);

-- Table: "product_promotion"

CREATE TABLE "product_promotion"
(
  product_promotion_id serial NOT NULL,
  ref_id_product INTEGER NOT NULL,
  ref_id_promotion INTEGER NOT NULL,
  date_start date NOT NULL,
  date_stop date NOT NULL,
  CONSTRAINT product_promotion_id PRIMARY KEY (product_promotion_id),
  CONSTRAINT ref_id_product FOREIGN KEY (ref_id_product)
    REFERENCES "product" (product_id) MATCH SIMPLE
    ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT ref_id_promotion FOREIGN KEY (ref_id_promotion)
    REFERENCES "local_promotion" (local_promotion_id) MATCH SIMPLE
    ON UPDATE CASCADE ON DELETE CASCADE
);

