INSERT INTO "rule" ("montant", "resultat", "unit") VALUES (10, NULL , 'euro');
INSERT INTO "rule" ("montant", "resultat", "unit") VALUES (20, NULL , 'poucent');
INSERT INTO "rule" ("montant", "resultat", "unit") VALUES (30, 100, 'euro');
INSERT INTO "rule" ("montant", "resultat", "unit") VALUES (1, 2, 'unit');
INSERT INTO "local_promotion" ("ref_local_promotion", "designation", "ref_id_rule", "recurence") VALUES ('promo1', 'promo local 1, sans promo', NULL , FALSE);
INSERT INTO "local_promotion" ("ref_local_promotion", "designation", "ref_id_rule", "recurence") VALUES ('promo2', 'promo local 2, -10 euros', 1, FALSE);
INSERT INTO "local_promotion" ("ref_local_promotion", "designation", "ref_id_rule", "recurence") VALUES ('promo3', 'promo local 3, 20 poucent', 2, FALSE);
INSERT INTO "local_promotion" ("ref_local_promotion", "designation", "ref_id_rule", "recurence") VALUES ('promo4', 'promo local 4, 30 euros a partir de 100', 3, FALSE);
INSERT INTO "local_promotion" ("ref_local_promotion", "designation", "ref_id_rule", "recurence") VALUES ('promo5', 'promo local 5, 2 achete 1 offert', 4, FALSE);